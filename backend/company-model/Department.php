<?php 

class Department {

	public function __construct(){}

	public function upsert($db, $name, $token){
		if($name != null && $token != null){
			if(trim($name) != "" && trim($token) != ""){
				$name = mysqli_real_escape_string($db, $name);
				$token = mysqli_real_escape_string($db, $token);

				$queryString = "INSERT INTO departments (_name, _token) VALUES ('".$name."', '".$token."') ON DUPLICATE KEY UPDATE _name = '".$name."'";
				$query = mysqli_query($db, $queryString);
				if($query){ return $token; }
			}
		}

		return null;
	}

	public function find($db, $department_name = null){
		if($department_name != null && trim($department_name) != ""){
			$search = mysqli_real_escape_string($db, $department_name);
			$queryString = "SELECT a._name as 'department', a._token as 'token' FROM departments a WHERE a._name like '%".$search."%' AND a._name NOT like '<deleted>%</deleted>' ORDER BY a._created ASC";
		}
		else{ $queryString = "SELECT a._name as 'department', a._token as 'token' FROM departments a WHERE BINARY 1 AND a._name NOT like '<deleted>%</deleted>' ORDER BY a._created ASC"; }
		
		$query = mysqli_query($db, $queryString);
		$arr = array();
		if($query){
			while ($row = mysqli_fetch_array($query)) { 
				array_push($arr, array("department" => $row["department"], "token" => $row["token"]));
			}
		}

		return $arr;
	}

	public function delete($token){
		// fail silently
		return false;
	}
}

?>