<?php 

class Company {

	public function __construct(){}

	public function upsert($db, $name, $token){
		if($name != null && $token != null){
			if(trim($name) != "" && trim($token) != ""){
				$name = mysqli_real_escape_string($db, $name);
				$token = mysqli_real_escape_string($db, $token);

				$queryString = "INSERT INTO companies (_name, _token) VALUES ('".$name."', '".$token."') ON DUPLICATE KEY UPDATE _name = '".$name."'";
				$query = mysqli_query($db, $queryString);
				if($query){ return $token; }
			}
		}

		return null;
	}

	public function find($db, $company_name = null){
		if($company_name != null && trim($company_name) != ""){
			$search = mysqli_real_escape_string($db, $company_name);
			$queryString = "SELECT a._name as 'company', a._token as 'token' FROM companies a WHERE a._name like '%".$search."%' AND a._name NOT like '<deleted>%</deleted>' ORDER BY a._created ASC";
		}
		else{ $queryString = "SELECT a._name as 'company', a._token as 'token' FROM companies a WHERE BINARY 1 AND a._name NOT like '<deleted>%</deleted>' ORDER BY a._created ASC"; }
		
		$query = mysqli_query($db, $queryString);
		$arr = array();
		if($query){
			while ($row = mysqli_fetch_array($query)) { 
				array_push($arr, array("company" => $row["company"], "token" => $row["token"]));
			}
		}

		return $arr;
	}

	public function delete($token){
		// fail silently
		return false;
	}
}

?>