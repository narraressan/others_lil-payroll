<?php
	// NOTE! - NO SECURITY PROCEDURE ON REST ENDPOINTS HAS BEEN IMPLEMENTED

	// THIS IS YOUR MAIN CONTROLLER

	require "misc/AltoRouter.php";
	require "misc/TokenGenerator.php";
	require "misc/Enc-Dec.php";
	require "misc/ConnectDB.php";
	require "misc/Salary-Calculator.php";

	require "company-model/Company.php";
	require "company-model/Department.php";
	require "company-model/Position.php";

	require "computable_factor-model/Computable-Factor.php";

	require "user-model/User-credible.php";
	require "user-model/User-details.php";
	require "user-model/User-session.php";

	require "payslip-model/Payslip.php";


	// router instance
		$router = new AltoRouter(); // set router instance
		// $router->setBasePath("/others_lil-payroll/backend/controller.php");
		$router->setBasePath("/backend/controller.php");
		
		// Default Routers
		// note - router can only handle url on exp - "http://localhost:81/others_lil-payroll/backend/index.php/home"
		$router->map("GET", "/", function(){ echo "LIL - PAYROLL - app backend running!"; });

	// MISC FUNCTIONS
		$router->map("POST|PUT", "/generate-token", function(){
			$log = array();
			$token = null;

			try {
				$tokenizer = new Token(50); array_push($log, "Tokenizer initiated");
				$token = $tokenizer->createToken(); array_push($log, "Token retrieved");
			} 
			catch(Exception $e){ array_push($log, $e); }

			$response = array("status" => $log, "data" => $token);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST|PUT", "/encrypt-text", function(){
			$log = array();
			$encrypted = null;

			$toEncrypt = $_POST["password"];
			array_push($log, $_POST);

			try {
				$encryptor = new EncDec($toEncrypt); array_push($log, "EncDec initiated on - ".$toEncrypt);
				$secret = $encryptor->getSecret(); array_push($log, "Secret text retrieved");
			} 
			catch(Exception $e){ array_push($log, $e); }

			$response = array("status" => $log, "data" => $secret);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST|PUT", "/decrypt-text", function(){
			$log = array();

			$encrypted = $_POST["encrypted"];
			$toCheck = $_POST["password"];
			array_push($log, $_POST);

			try {
				$encryptor = new EncDec(); array_push($log, "EncDec initiated");
				$isSecret = $encryptor->checkSecret($encrypted, $toCheck); array_push($log, "Secret text compatibility checked");
			} 
			catch(Exception $e){ array_push($log, $e); }

			$response = array("status" => $log, "data" => $isSecret);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST", "/compute-salary", function(){
			$log = array();
			$salaryData = array();

			try {
				$calc = new SalaryCalculator($_POST["basic_salary"], $_POST["taxable_incentives"], $_POST["remaining_months"]); array_push($log, "Calculator initiated");

				$grossIncome = $calc->getGrossIncome(); array_push($log, "Gross Income: ".$grossIncome);
				$taxableIncome = $calc->getTaxableIncome($grossIncome); array_push($log, "Taxable Income: ".$taxableIncome);
				$taxFormula = $calc->taxTable($taxableIncome); array_push($log, "Tax formula: ".$taxFormula);
				$monthlyTax = eval($taxFormula); array_push($log, "Monthly Tax: ".$monthlyTax);
				
				$salaryData = array(
							"gross-income"=>$grossIncome,
							"taxable-income"=>$taxableIncome,
							"tax-formula"=>$taxFormula,
							"monthly-tax"=>$monthlyTax,
						);
			}
			catch(Exception $e){ array_push($log, $e); }

			$response = array("status" => $log, "data" => $salaryData);
			header("Content-Type: application/json");
			echo json_encode($response);
		});



	// CRUD - company
		$router->map("GET", "/get-companies", function(){
			$db = new ConnectDB();
			$companies = new Company();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$companies = $companies->find($openDB, $_GET["search"]); array_push($log, "find: ".$_GET["search"]);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $companies);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST", "/upsert-company", function(){
			$db = new ConnectDB();
			$companies = new Company();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");

				array_push($log, $_POST);
				$name = $_POST["company"];

				if($_POST["token"] == null || trim($_POST["token"]) == ""){
					$tokenizer = new Token(50); array_push($log, "Tokenizer initiated");
					$token = $tokenizer->createToken(); array_push($log, "Token retrieved");
				}
				else{ $token = $_POST["token"]; }

				$companies = $companies->upsert($openDB, $name, $token); array_push($log, "upsert: ".$token);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $companies);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

	// CRUD - department
		$router->map("GET", "/get-departments", function(){
			$db = new ConnectDB();
			$departments = new Department();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$departments = $departments->find($openDB, $_GET["search"]); array_push($log, "find: ".$_GET["search"]);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $departments);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST", "/upsert-department", function(){
			$db = new ConnectDB();
			$departments = new Department();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");

				array_push($log, $_POST);
				$name = $_POST["department"];

				if($_POST["token"] == null || trim($_POST["token"]) == ""){
					$tokenizer = new Token(50); array_push($log, "Tokenizer initiated");
					$token = $tokenizer->createToken(); array_push($log, "Token retrieved");
				}
				else{ $token = $_POST["token"]; }

				$departments = $departments->upsert($openDB, $name, $token); array_push($log, "upsert: ".$token);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $departments);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

	// CRUD - position
		$router->map("GET", "/get-positions", function(){
			$db = new ConnectDB();
			$positions = new Position();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$positions = $positions->find($openDB, $_GET["search"]); array_push($log, "find: ".$_GET["search"]);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $positions);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST", "/upsert-position", function(){
			$db = new ConnectDB();
			$positions = new Position();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");

				array_push($log, $_POST);
				$name = $_POST["position"];

				if($_POST["token"] == null || trim($_POST["token"]) == ""){
					$tokenizer = new Token(50); array_push($log, "Tokenizer initiated");
					$token = $tokenizer->createToken(); array_push($log, "Token retrieved");
				}
				else{ $token = $_POST["token"]; }

				$positions = $positions->upsert($openDB, $name, $token); array_push($log, "upsert: ".$token);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $positions);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

	// CRUD - computable_factors
		$router->map("GET", "/get-computable-factors", function(){
			$db = new ConnectDB();
			$positions = new ComputabeFactor();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$positions = $positions->find($openDB, $_GET["search"]); array_push($log, "find: ".$_GET["search"]);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $positions);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("GET", "/get-deductions-factors", function(){
			$db = new ConnectDB();
			$positions = new ComputabeFactor();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$positions = $positions->find_deductions($openDB, $_GET["search"]); array_push($log, "find: ".$_GET["search"]);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $positions);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("GET", "/get-incentive-factors", function(){
			$db = new ConnectDB();
			$positions = new ComputabeFactor();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$positions = $positions->find_incentives($openDB, $_GET["search"]); array_push($log, "find: ".$_GET["search"]);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $positions);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST", "/upsert-computable-factor", function(){
			$db = new ConnectDB();
			$positions = new ComputabeFactor();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");

				array_push($log, $_POST);
				$name = $_POST["factor"];
				$isDeduction = $_POST["status"];

				if($_POST["token"] == null || trim($_POST["token"]) == ""){
					$tokenizer = new Token(50); array_push($log, "Tokenizer initiated");
					$token = $tokenizer->createToken(); array_push($log, "Token retrieved");
				}
				else{ $token = $_POST["token"]; }

				$positions = $positions->upsert($openDB, $name, $isDeduction, $token); array_push($log, "upsert: ".$token);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $positions);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

	// CRUD - users
		$router->map("POST", "/upsert-user-credible", function(){
			$db = new ConnectDB();
			$users = new UserCredible();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");

				array_push($log, $_POST);
				$email = $_POST["email"];
				$password = $_POST["password"];

				if($_POST["token"] == null || trim($_POST["token"]) == ""){
					$tokenizer = new Token(50); array_push($log, "Tokenizer initiated");
					$token = $tokenizer->createToken(); array_push($log, "Token retrieved");
				}
				else{ $token = $_POST["token"]; }

				$encryptor = new EncDec($password); array_push($log, "EncDec initiated on - ".$password);
				$password = $encryptor->getSecret(); array_push($log, "Secret text retrieved");

				$users = $users->upsert($openDB, $email, $password, $token); array_push($log, "upsert: ".$token);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $users);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST", "/upsert-user-details", function(){
			$db = new ConnectDB();
			$users = new UserDetails();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");

				array_push($log, $_POST);
				$firstname = $_POST["firstname"];
				$lastname = $_POST["lastname"];
				$employee_id = $_POST["employee_id"];
				$company = $_POST["company"];
				$department = $_POST["department"];
				$position = $_POST["position"];
				$credibles = $_POST["credibles"];

				if($_POST["token"] == null || trim($_POST["token"]) == ""){
					$tokenizer = new Token(50); array_push($log, "Tokenizer initiated");
					$token = $tokenizer->createToken(); array_push($log, "Token retrieved");
				}
				else{ $token = $_POST["token"]; }

				$users = $users->upsert($openDB, $firstname, $lastname, $employee_id, $company, $department, $position, $credibles, $token); array_push($log, "upsert: ".$token);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $users);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST", "/user-login", function(){
			$db = new ConnectDB();
			$usrData = new UserSession();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$usrData = $usrData->login($openDB, $_POST["email"]); array_push($log, "logging: ".$_POST["email"]);

				if(count($usrData) > 0){
					$encryptor = new EncDec(); array_push($log, "EncDec initiated");
					$isSecret = $encryptor->checkSecret($usrData["password"], $_POST["password"]); array_push($log, "Secret text compatibility checked");
					if($isSecret != true){ $usrData = null; }
				}
				else{ $usrData = null; }
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $usrData);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("GET", "/get-users", function(){
			$db = new ConnectDB();
			$usrData = new UserSession();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$usrData = $usrData->find($openDB);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $usrData);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST", "/deactivate-users", function(){
			$db = new ConnectDB();
			$users = new UserCredible();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");

				array_push($log, $_POST);
				$active = $_POST["active"];
				$token = $_POST["token"];

				$users = $users->deactivateUser($openDB, $active, $token); array_push($log, "upsert: ".$token);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $users);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("GET", "/get-users-by-company", function(){
			$db = new ConnectDB();
			$usrData = new UserSession();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$usrData = $usrData->find($openDB);

				$companyEmployees = array();
				for ($i=0; $i < count($usrData); $i++) { 
					if($usrData[$i]["_company"] == $_GET["company"]){ array_push($companyEmployees, $usrData[$i]); }
				}

				$usrData = $companyEmployees;
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $usrData);
			header("Content-Type: application/json");
			echo json_encode($response);
		});



	// PROCESS PAYSLIP
		$router->map("POST", "/create-payroll", function(){
			$db = new ConnectDB();
			$payslip = new Payslip();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");

				array_push($log, $_POST);
				$billing_schedule = $_POST["billing_schedule"];
				$billing_coverage = $_POST["billing_coverage"];
				$employees = $_POST["employees"];

				$employee_token = array();

				for ($i=0; $i < count($employees); $i++) { 
					// attach tokens to all employees
					$tokenizer = new Token(50); array_push($log, "Tokenizer initiated");

					$temp = array("_employee" => $employees[$i], "_token" => $tokenizer->createToken());
					array_push($employee_token, $temp);
				}

				$payslip = $payslip->insert($openDB, $billing_schedule, $billing_coverage, $employee_token); array_push($log, "upsert: ".json_encode($employee_token));
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $payslip);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("GET", "/get-payrolls", function(){
			$db = new ConnectDB();
			$payrolls = new Payslip();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$payrolls = $payrolls->find_payroll_by_company_withPrev($openDB, $_GET["company"]); array_push($log, "find: ".$_GET["company"]);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $payrolls);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("GET", "/get-payroll-filters", function(){
			$db = new ConnectDB();
			$payrolls = new Payslip();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$payrolls = $payrolls->find_payroll_by_company_filters($openDB, $_GET["company"]); array_push($log, "find: ".$_GET["company"]);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $payrolls);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST", "/update-basic-salary", function(){
			$db = new ConnectDB();
			$payslip = new Payslip();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");

				array_push($log, $_POST);
				$basic = $_POST["basic"];
				$token = $_POST["payslip"];
				$payslip = $payslip->update_basicSalary($openDB, $basic, $token); array_push($log, "upsert: ".$token);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $payslip);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST", "/update-tax", function(){
			$db = new ConnectDB();
			$payslip = new Payslip();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");

				array_push($log, $_POST);
				$tax = $_POST["tax"];
				$net = $_POST["net"];
				$token = $_POST["payslip"];
				$payslip = $payslip->update_Tax($openDB, $tax, $net, $token); array_push($log, "upsert: ".$token);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $payslip);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("POST", "/upsert-computable-per-sched", function(){
			$db = new ConnectDB();
			$payslip = new Payslip();
			$log = array();

			$payslipArr = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");

				array_push($log, $_POST);
				$payslip_id = $_POST["payslip"];
				$factors = $_POST["factors"];

				for ($i=0; $i < count($factors); $i++) { 
					$tokenizer = new Token(50); array_push($log, "Tokenizer initiated");
					$token = $tokenizer->createToken(); array_push($log, "Token retrieved");

					$amt = $factors[$i]["value"];
					$factor = $factors[$i]["name"];

					if($amt != "" && $amt != null){
						$upsertPayslip = $payslip->upsertComputable_perSchedule($openDB, $amt, $factor, $payslip_id, $token); 
						array_push($log, "upsert: ".$token);
						array_push($payslipArr, $upsertPayslip);
					}
				}
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $payslipArr);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("GET", "/get-payslip-factors", function(){
			$db = new ConnectDB();
			$payrolls = new Payslip();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$payrolls = $payrolls->getExistingComputables($openDB, $_GET["payslip"]); array_push($log, "find: ".$_GET["payslip"]);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $payrolls);
			header("Content-Type: application/json");
			echo json_encode($response);
		});

		$router->map("GET", "/get-my-payslips", function(){
			$db = new ConnectDB();
			$payrolls = new Payslip();
			$log = array();

			try {
				$openDB = $db->openDB(); array_push($log, "DB - open for connection");
				$payrolls = $payrolls->getMyPayslips($openDB, $_GET["employee"]); array_push($log, "find: ".$_GET["employee"]);
			} 
			catch(Exception $e){ array_push($log, $e); }

			$db->closeDB($openDB); array_push($log, "DB - connection closed");
			$response = array("status" => $log, "data" => $payrolls);
			header("Content-Type: application/json");
			echo json_encode($response);
		});







	// router configurations
		$match = $router->match();

		// direct to proper endpoint
		if($match && is_callable($match["target"])){ call_user_func_array($match["target"], $match["params"]);}
		// return file - etc. img, html, css, or download-able files
		else if($match && is_file($match["target"])){ require $match["target"]; }
		// if endpoint does not exist 
		else{ echo "404"; }
?>