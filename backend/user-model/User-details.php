<?php 

class UserDetails {

	public function __construct(){}

	public function upsert($db, $firstname, $lastname, $employee_id, $company = null, $department = null, $position = null, $credibles, $token){
		if($credibles != null && $token != null){
			if(trim($credibles) != "" && trim($token) != ""){
				$firstname = mysqli_real_escape_string($db, $firstname);
				$lastname = mysqli_real_escape_string($db, $lastname);
				$employee_id = mysqli_real_escape_string($db, $employee_id);
				$credibles = mysqli_real_escape_string($db, $credibles);
				$token = mysqli_real_escape_string($db, $token);

				$company = mysqli_real_escape_string($db, $company);
				$department = mysqli_real_escape_string($db, $department);
				$position = mysqli_real_escape_string($db, $position);

				if(trim($company) == ""){ $company = "null"; }
				else{ $company = "'".$company."'"; }

				if(trim($department) == ""){ $department = "null"; }
				else{ $department = "'".$department."'"; }

				if(trim($position) == ""){ $position = "null"; }
				else{ $position = "'".$position."'"; }

				$queryString = "INSERT INTO user_details (firstname, lastname, employee_id, _company, _department, _position, _credibles, _token) VALUES ('".$firstname."', '".$lastname."', '".$employee_id."', ".$company.", ".$department.", ".$position.", '".$credibles."', '".$token."') ON DUPLICATE KEY UPDATE firstname = '".$firstname."', lastname = '".$lastname."', employee_id = '".$employee_id."', _company = ".$company.", _department = ".$department.", _position = ".$position;
				$query = mysqli_query($db, $queryString);
				if($query){ return $token; }
			}
		}

		return null;
	}

	public function delete($token){
		// fail silently
		return false;
	}
}

?>