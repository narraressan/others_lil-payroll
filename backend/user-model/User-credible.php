<?php 

class UserCredible {

	public function __construct(){}

	public function upsert($db, $email, $password, $token){
		if($email != null && $password != null && $token != null){
			if(trim($email) != "" && trim($password) != "" && trim($token) != ""){
				$email = mysqli_real_escape_string($db, $email);
				$password = mysqli_real_escape_string($db, $password);
				$token = mysqli_real_escape_string($db, $token);

				$queryString = "INSERT INTO user_credible (email, password, _token) VALUES ('".$email."', '".$password."', '".$token."') ON DUPLICATE KEY UPDATE email = '".$email."', password = '".$password."'";
				$query = mysqli_query($db, $queryString);
				if($query){ return $token; }
			}
		}

		return null;
	}

	public function deactivateUser($db, $status, $token){
		if($token != null){
			if(trim($token) != ""){
				$token = mysqli_real_escape_string($db, $token);

				$queryString = "UPDATE user_credible SET _active = ".$status." WHERE _token = '".$token."'";
				$query = mysqli_query($db, $queryString);
				if($query){ return $token; }
			}
		}

		return null;
	}

	public function delete($token){
		// fail silently
		return false;
	}
}

?>