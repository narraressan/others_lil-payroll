<?php 

class UserSession {

	public function __construct(){}

	public function login($db, $email){
		if($email != null && trim($email) != ""){
			$email = mysqli_real_escape_string($db, $email);

			$queryString = "SELECT * FROM view_users a WHERE BINARY a.email = '".$email."' AND _active=1";
		}
		
		$query = mysqli_query($db, $queryString);
		$arr = array();
		if($query){
			if(mysqli_num_rows($query) <= 1){
				while ($row = mysqli_fetch_array($query)) { 
					$arr = array(
								"_login" => $row["_login"],
								"_user" => $row["_user"],
								"email" => $row["email"],
								"password" => $row["password"],
								"firstname" => $row["firstname"],
								"lastname" => $row["lastname"],
								"employee_id" => $row["employee_id"],
								"_company" => $row["_company"],
								"company" => $row["company"],
								"_department" => $row["_department"],
								"department" => $row["department"],
								"_position" => $row["_position"],
								"position" => $row["position"],
							);
				}
			}
		}

		return $arr;
	}

	public function find($db){
		$queryString = "SELECT * FROM view_users c LEFT JOIN (SELECT b._id as prev_id, b._employee as prev_employee, b.tax as prev_tax, b._basic_salary as prev_salary, b.billing_schedule, b.billing_coverage FROM scheduled_payslip b WHERE b._id NOT IN (SELECT max(a._id) FROM scheduled_payslip a GROUP BY a._employee)) as d ON d.prev_employee = c._user WHERE 1 AND c._active=1 GROUP BY c._user, c._login ORDER BY c._log ASC";

		$query = mysqli_query($db, $queryString);
		$arr = array();
		if($query){
			while ($row = mysqli_fetch_array($query)) { 
				array_push($arr, array(
							"_login" => $row["_login"],
							"_user" => $row["_user"],
							"email" => $row["email"],
							"password" => $row["password"],
							"firstname" => $row["firstname"],
							"lastname" => $row["lastname"],
							"employee_id" => $row["employee_id"],
							"_company" => $row["_company"],
							"company" => $row["company"],
							"_department" => $row["_department"],
							"department" => $row["department"],
							"_position" => $row["_position"],
							"position" => $row["position"],
							"prev_tax" => $row["prev_tax"],
							"prev_salary" => $row["prev_salary"],
							"last_billing" => $row["billing_schedule"],
							"last_coverage" => $row["billing_coverage"],
						));
			}
		}

		return $arr;
	}

	public function delete($token){
		// fail silently
		return false;
	}
}

?>