
<!-- Dependencies -->
1. AltoRouter - http://altorouter.com/




<!-- REF -->
Intro to PHP Classes - http://codular.com/introducing-php-classes


<!-- notes -->
As much as possible - add only 1 company per database setup


VIEW - view_users
select DISTINCT a._token as '_login', b._token as '_user', a.email, a.password, b.firstname, b.lastname, b.employee_id, b._company, c._name as 'company', b._department, d._name as 'department', b._position, e._name as 'position', a._active as '_active', a._created as '_log'
from (((user_credible a left join user_details b on a._token = b._credibles) LEFT JOIN companies c on b._company = c._token) LEFT JOIN departments d on b._department = d._token) LEFT JOIN positions e on b._position = e._token;

VIEW - view_deductables
SELECT a._name as 'deduction', a._token FROM computable_factors a where a._is_dedctn_not_incntv = true;

VIEW - view_incentives
SELECT a._name as 'incentive', a._token FROM computable_factors a where a._is_dedctn_not_incntv = false;

VIEW -`view_payrolls`
SELECT DISTINCT b._token as 'payslip_id', a.*, b.billing_schedule as 'billing', b.billing_coverage as 'coverage', b._basic_salary as 'gross_salary', b.net_salary as 'net', b.tax, b._created as 'payslip_generated_on' from view_users a, scheduled_payslip b WHERE a._user = b._employee;

VIEW - view_payslip_factors
SELECT DISTINCT a._employee as 'employee', a._basic_salary as 'basic', a._token as 'payslip_id', b._token as 'cmptbl_psched_id', b._factor as 'factor_id', b.amount as 'amt', c._name as 'factor', c._is_dedctn_not_incntv as 'type' FROM scheduled_payslip a, computable_per_schedule b, computable_factors c WHERE a._token = b._payslip AND b._factor = c._token;


<!-- admin user -->
admin@admin - admin