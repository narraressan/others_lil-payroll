<?php 

class ComputabeFactor {

	public function __construct(){}

	public function upsert($db, $name, $is_dedctn_not_incntv, $token){
		if($name != null && $token != null){
			if(trim($name) != "" && trim($token) != ""){
				$name = mysqli_real_escape_string($db, $name);
				$is_dedctn_not_incntv = mysqli_real_escape_string($db, $is_dedctn_not_incntv);
				$token = mysqli_real_escape_string($db, $token);

				$queryString = "INSERT INTO computable_factors (_name, _is_dedctn_not_incntv, _token) VALUES ('".$name."',".$is_dedctn_not_incntv.", '".$token."') ON DUPLICATE KEY UPDATE _name = '".$name."', _is_dedctn_not_incntv = '".$is_dedctn_not_incntv."'";
				$query = mysqli_query($db, $queryString);
				if($query){ return $token; }
			}
		}

		return null;
	}

	public function find($db, $factor = null){
		if($factor != null && trim($factor) != ""){
			$search = mysqli_real_escape_string($db, $factor);
			$queryString = "SELECT a._name as 'factor', a._token as 'token', a._is_dedctn_not_incntv as 'type' FROM computable_factors a WHERE a._name like '%".$search."%' AND a._name NOT like '<deleted>%</deleted>' ORDER BY a._created ASC";
		}
		else{ $queryString = "SELECT a._name as 'factor', a._token as 'token', a._is_dedctn_not_incntv as 'type' FROM computable_factors a WHERE BINARY 1 AND a._name NOT like '<deleted>%</deleted>' ORDER BY a._created ASC"; }
		
		$query = mysqli_query($db, $queryString);
		$arr = array();
		if($query){
			while ($row = mysqli_fetch_array($query)) { 
				$staus = null;
				if($row["type"] == true){ $staus = "deduction"; }
				else{ $staus = "incentive"; }

				array_push($arr, array("factor" => $row["factor"], "status" => $staus, "_status" => $row["type"], "token" => $row["token"]));
			}
		}

		return $arr;
	}

	public function find_deductions($db, $factor = null){
		if($factor != null && trim($factor) != ""){
			$search = mysqli_real_escape_string($db, $factor);
			$queryString = "SELECT a.deduction, a._token as 'token' FROM view_deductables a WHERE a.deduction like '%".$search."%' AND a.deduction NOT like '<deleted>%</deleted>'";
		}
		else{ $queryString = "SELECT a.deduction, a._token as 'token' FROM view_deductables a WHERE BINARY 1 AND a.deduction NOT like '<deleted>%</deleted>'"; }
		
		$query = mysqli_query($db, $queryString);
		$arr = array();
		if($query){
			while ($row = mysqli_fetch_array($query)) { 
				array_push($arr, array("deduction" => $row["deduction"], "token" => $row["token"]));
			}
		}

		return $arr;
	}

	public function find_incentives($db, $factor = null){
		if($factor != null && trim($factor) != ""){
			$search = mysqli_real_escape_string($db, $factor);
			$queryString = "SELECT a.incentive, a._token as 'token' FROM view_incentives a WHERE a.incentive like '%".$search."%' AND a.incentive NOT like '<deleted>%</deleted>'";
		}
		else{ $queryString = "SELECT a.incentive, a._token as 'token' FROM view_incentives a WHERE BINARY 1 AND a.incentive NOT like '<deleted>%</deleted>'"; }
		
		$query = mysqli_query($db, $queryString);
		$arr = array();
		if($query){
			while ($row = mysqli_fetch_array($query)) { 
				array_push($arr, array("incentive" => $row["incentive"], "token" => $row["token"]));
			}
		}

		return $arr;
	}

	public function delete($token){
		// fail silently
		return false;
	}
}

?>