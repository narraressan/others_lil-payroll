-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2017 at 04:12 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lilpayroll`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `_id` int(11) NOT NULL,
  `_name` varchar(100) NOT NULL,
  `_token` varchar(50) NOT NULL,
  `_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`_id`, `_name`, `_token`, `_created`) VALUES
(3, 'KCC - Mall of GSC', '3DtUWQnB8RDq38jzyH2LXXWb2UvDg_VgocKPO12vjpu3iO5HAc', '2017-09-28 05:44:53'),
(2, 'Narra IT Solutions', '8IrQcCgBPN7pjG6uzeQyUZmO8Pi_GfLikfitqh2JQjzMTO_QqK', '2017-09-27 10:03:43'),
(23, '<deleted>new Company</deleted>', 'e0R-yejmAb1PiAfWVciixEiUIOkq5FcFlNJMU6fwXcjm2C2q7D', '2017-09-30 22:35:34'),
(20, 'SM Mall of GSC', 'voErt7D7jT6eElqgxNp9BTqoUTiX2tog9UxlOS3D3BR6zOcenJ', '2017-09-28 05:57:51'),
(1, 'MSU - GSC', 'vqZTS0coyiSw90iKMml0Z3mQSla82W7pC04D9lmeM6Pd13O33o', '2017-09-27 10:03:43'),
(5, 'samatosa Inc 2', 'y22_z5FzkGPxiTJLtj7yh3eMJJLMfoVnRFqYZCxkBSVRbvPmI5', '2017-09-28 05:47:05'),
(6, 'samatosa Inc 3', 'YGmW-mtoFfmO8c-LazSkZ8U8ejBgdWViCijlFhN_2dNk0ZhYhT', '2017-09-28 05:48:57'),
(9, '<deleted>samatosa Inc 5</deleted>', 'YGmW-mtoFfmO8c-LazSkZ8U8ejBgdWViCijlFhN_2dNk0ZhYyy', '2017-09-28 05:50:29'),
(12, '<deleted>samatosa Inc 6</deleted>', '_X_xn6H9i79bAYZjA3eM7qxgzvY1Lf5M093JccsAGQlBf0ksie', '2017-09-28 05:51:36');

-- --------------------------------------------------------

--
-- Table structure for table `computable_factors`
--

CREATE TABLE `computable_factors` (
  `_id` int(11) NOT NULL,
  `_name` varchar(100) NOT NULL,
  `_is_dedctn_not_incntv` tinyint(1) NOT NULL DEFAULT '0',
  `_token` varchar(50) NOT NULL,
  `_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `computable_factors`
--

INSERT INTO `computable_factors` (`_id`, `_name`, `_is_dedctn_not_incntv`, `_token`, `_created`) VALUES
(12, '<deleted>sample factor</deleted>', 0, '9VOWHrmw9AukjTYyEc144-G3RGW15PgJZs-U5fqLr0DCgpuD3r', '2017-10-01 02:11:25'),
(1, 'SSS', 1, 'aVk1TW8W9Ks6KHJGQEXd9WyQWnx5Tmn7cI0vEMWkpn5bdT_ai7', '2017-09-27 10:14:31'),
(9, 'allowance', 0, 'dcLXEZXHNIcBLFoE8wSg1xrs0ouGbkQT93Pz-PiGkhCIdJBBhP', '2017-09-28 06:53:25'),
(4, 'Pag-ibig', 1, 'DQhSe0EC92CAS0pBixL-ZHSFP2sUkKKysls_GYO1Y9v2M2NS4e', '2017-09-27 10:14:31'),
(6, 'overtime', 0, 'IrWY7oO7o64FtasVQH9IreVoMVWQMiVlWw6Nd9dv5iukxxBD-Y', '2017-09-27 10:14:31'),
(5, 'tardy', 1, 'mPlOsMzlN2aYyv9dvu-fQQEDgUys1vJjR-CiWfa0_AVyeQLuqm', '2017-09-27 10:14:31'),
(7, 'holiday pay', 0, 'NdsaMsogMbLRuL1_a_aiwfFRMnsfCJ1Ch6mvvUU0uWDMXx-Q6A', '2017-09-27 10:14:31'),
(3, 'Philhealth', 1, 'OA7fxN0sPo7hSdplnEmJCvW-f7AiymibbyyNpyiFWI-VYRF8cj', '2017-09-27 10:14:31'),
(2, 'GSIS', 1, 'yUmshqCI7mOezP6Uzmh_B6xoDDAr62E_2WrmTnaqA-BvDTUr_B', '2017-09-27 10:14:31');

-- --------------------------------------------------------

--
-- Table structure for table `computable_per_schedule`
--

CREATE TABLE `computable_per_schedule` (
  `_id` int(11) NOT NULL,
  `_token` varchar(50) NOT NULL,
  `_payslip` varchar(50) NOT NULL,
  `_factor` varchar(50) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `_remarks` text NOT NULL,
  `_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `computable_per_schedule`
--

INSERT INTO `computable_per_schedule` (`_id`, `_token`, `_payslip`, `_factor`, `amount`, `_remarks`, `_created`) VALUES
(24, 'NEzXzkgvtA3WPS3qt8QJMTv1z3D4b8k3v5cKQokh0-irpocqSx', 'p0JR7troUFIu24ClsGJHrX9JRPwb1NThQPQKBntM9rp5SsJXzq', 'aVk1TW8W9Ks6KHJGQEXd9WyQWnx5Tmn7cI0vEMWkpn5bdT_ai7', 0, '', '2017-10-03 06:07:48'),
(1, 'lSTQiFwoXb81JxYCDhIs7yApbVjPht7xnPKVW1mLWKJrP-0PBq', 'p0JR7troUFIu24ClsGJHrX9JRPwb1NThQPQKBntM9rp5SsJXzq', 'dcLXEZXHNIcBLFoE8wSg1xrs0ouGbkQT93Pz-PiGkhCIdJBBhP', 0, '', '2017-10-03 06:08:26'),
(4, '-3J6sXstwJBBGwokUVx0E-fwUnga57OPfSyNOFMWJ68BukWD3U', 'p0JR7troUFIu24ClsGJHrX9JRPwb1NThQPQKBntM9rp5SsJXzq', 'DQhSe0EC92CAS0pBixL-ZHSFP2sUkKKysls_GYO1Y9v2M2NS4e', 200, '', '2017-10-03 05:05:11'),
(2, '-cI6Ss7EkWk6Drg7tlrN4vNEaeOQYoqXak820TWLdoe8-I2Q49', 'p0JR7troUFIu24ClsGJHrX9JRPwb1NThQPQKBntM9rp5SsJXzq', 'IrWY7oO7o64FtasVQH9IreVoMVWQMiVlWw6Nd9dv5iukxxBD-Y', 0, '', '2017-10-03 06:08:26'),
(5, 'OnWV-G1YfSS7MOm0fP0KYLPTzZbwki7fX55aDb2U6WuuC9lzQd', 'p0JR7troUFIu24ClsGJHrX9JRPwb1NThQPQKBntM9rp5SsJXzq', 'mPlOsMzlN2aYyv9dvu-fQQEDgUys1vJjR-CiWfa0_AVyeQLuqm', 300, '', '2017-10-03 05:05:12'),
(3, 'PUGCxXvoE2Ml7PdQtl2GCd_e0yflHVzl4o858CIWnEsGJ48IF1', 'p0JR7troUFIu24ClsGJHrX9JRPwb1NThQPQKBntM9rp5SsJXzq', 'NdsaMsogMbLRuL1_a_aiwfFRMnsfCJ1Ch6mvvUU0uWDMXx-Q6A', 200, '', '2017-10-03 05:05:11'),
(6, '7c0YM0SJL3Cl2ZYb7Q74YdRm_ekUduh-52YyCw1Rc-MNS4RF2S', 'p0JR7troUFIu24ClsGJHrX9JRPwb1NThQPQKBntM9rp5SsJXzq', 'OA7fxN0sPo7hSdplnEmJCvW-f7AiymibbyyNpyiFWI-VYRF8cj', 150, '', '2017-10-03 06:07:48'),
(28, 'oh380dBysEpf22BtCnLVw7-PCeUTdbykbYyVS-_O5itKei5W5-', 'p0JR7troUFIu24ClsGJHrX9JRPwb1NThQPQKBntM9rp5SsJXzq', 'yUmshqCI7mOezP6Uzmh_B6xoDDAr62E_2WrmTnaqA-BvDTUr_B', 0, '', '2017-10-03 06:07:48'),
(40, 'S8z4KjQO0RVSo5gn5UTWRAcBS0c_Rxmc2ZFR9Hx_qwZdxPrEJp', 'RFKVe_D70w862fUzFDYKIEWjvKVyXUu7jAMhvLkKRVgI9_aUPu', 'aVk1TW8W9Ks6KHJGQEXd9WyQWnx5Tmn7cI0vEMWkpn5bdT_ai7', 233, '', '2017-10-03 10:20:14'),
(37, 'MJP6AC5WUOGT9mMQnV56_wSzZNPyPP8UsbAYm0lzxT93qSej_A', 'RFKVe_D70w862fUzFDYKIEWjvKVyXUu7jAMhvLkKRVgI9_aUPu', 'dcLXEZXHNIcBLFoE8wSg1xrs0ouGbkQT93Pz-PiGkhCIdJBBhP', 0, '', '2017-10-03 10:20:13'),
(41, 'USIEQYInbxqXtG6aC_sr5xIzUXyVtidiObr4kIq63sTJrw2Msu', 'RFKVe_D70w862fUzFDYKIEWjvKVyXUu7jAMhvLkKRVgI9_aUPu', 'DQhSe0EC92CAS0pBixL-ZHSFP2sUkKKysls_GYO1Y9v2M2NS4e', 200, '', '2017-10-03 10:20:14'),
(38, 'iEJ6yUz2Kq-v24-2da1OtA3jwDyKAauCnyj36C0HxH3zQDqhKM', 'RFKVe_D70w862fUzFDYKIEWjvKVyXUu7jAMhvLkKRVgI9_aUPu', 'IrWY7oO7o64FtasVQH9IreVoMVWQMiVlWw6Nd9dv5iukxxBD-Y', 0, '', '2017-10-03 10:20:13'),
(42, 'DzytS-I2rnokBzzU2VJQ0dOKcTiKSJ9nG4SNTOnxcdaJRcdYx5', 'RFKVe_D70w862fUzFDYKIEWjvKVyXUu7jAMhvLkKRVgI9_aUPu', 'mPlOsMzlN2aYyv9dvu-fQQEDgUys1vJjR-CiWfa0_AVyeQLuqm', 400, '', '2017-10-03 10:20:14'),
(39, 'zXtZs8LAzZOpR1jQOpyHmHbj0_uyKT7K_tlKKlSUrBU15Q8v87', 'RFKVe_D70w862fUzFDYKIEWjvKVyXUu7jAMhvLkKRVgI9_aUPu', 'NdsaMsogMbLRuL1_a_aiwfFRMnsfCJ1Ch6mvvUU0uWDMXx-Q6A', 500, '', '2017-10-03 10:20:14'),
(43, 'ZktIqppq0jy2KEBY3sNZXoRsQvZeKpUF6bPAVsukDh6Lcgoc7w', 'RFKVe_D70w862fUzFDYKIEWjvKVyXUu7jAMhvLkKRVgI9_aUPu', 'OA7fxN0sPo7hSdplnEmJCvW-f7AiymibbyyNpyiFWI-VYRF8cj', 0, '', '2017-10-03 10:20:14'),
(44, 'zwmCm2nFZD8e7B6r1PhTrC5DyQxh3Q3YdrFdK0NaRopqdV-37q', 'RFKVe_D70w862fUzFDYKIEWjvKVyXUu7jAMhvLkKRVgI9_aUPu', 'yUmshqCI7mOezP6Uzmh_B6xoDDAr62E_2WrmTnaqA-BvDTUr_B', 0, '', '2017-10-03 10:20:14'),
(16, 'uBgjGqrQCWkjDA20znbDNLQo6Hz-VFuH-CcaK2OGZVTU1Hw4Mk', 'uNlQgFeSSJ30SJ9MnRm4_Q0qOU_MoLCzSDIPPeEYkNg_-0VlX7', 'aVk1TW8W9Ks6KHJGQEXd9WyQWnx5Tmn7cI0vEMWkpn5bdT_ai7', 120, '', '2017-10-03 06:06:08'),
(13, 'fzccmQ9H7hE1F6uax22ASj9fdD9EIcVIWMKXYwuhocwdl0GJoH', 'uNlQgFeSSJ30SJ9MnRm4_Q0qOU_MoLCzSDIPPeEYkNg_-0VlX7', 'dcLXEZXHNIcBLFoE8wSg1xrs0ouGbkQT93Pz-PiGkhCIdJBBhP', 0, '', '2017-10-03 06:06:08'),
(17, 'kWxjnG9Ch0-H8bUwxVayyoINOlPFm0maQ5FMjQBqoVtCaz9hew', 'uNlQgFeSSJ30SJ9MnRm4_Q0qOU_MoLCzSDIPPeEYkNg_-0VlX7', 'DQhSe0EC92CAS0pBixL-ZHSFP2sUkKKysls_GYO1Y9v2M2NS4e', 200, '', '2017-10-03 06:06:08'),
(14, 'gfEraNF_JMAyMcYRExV4cK1-NhpOd8DcSNNrzaudGNPM-znNW0', 'uNlQgFeSSJ30SJ9MnRm4_Q0qOU_MoLCzSDIPPeEYkNg_-0VlX7', 'IrWY7oO7o64FtasVQH9IreVoMVWQMiVlWw6Nd9dv5iukxxBD-Y', 500, '', '2017-10-03 06:06:08'),
(18, '-XG_Pw4GZwAQjTJ8a4DRo5TGjozPOXi-bbari8LTwe9wEs_32h', 'uNlQgFeSSJ30SJ9MnRm4_Q0qOU_MoLCzSDIPPeEYkNg_-0VlX7', 'mPlOsMzlN2aYyv9dvu-fQQEDgUys1vJjR-CiWfa0_AVyeQLuqm', 150, '', '2017-10-03 06:06:08'),
(15, 'fwh41FpeQs8sdBQcjjksHk_FrSDPaCBmZQKhqcp2E_rDcSbud3', 'uNlQgFeSSJ30SJ9MnRm4_Q0qOU_MoLCzSDIPPeEYkNg_-0VlX7', 'NdsaMsogMbLRuL1_a_aiwfFRMnsfCJ1Ch6mvvUU0uWDMXx-Q6A', 0, '', '2017-10-03 06:06:08'),
(19, 'iLzFKjy2lXQsLmLk1NaDOYqR74lWCSilmx64YQ8BvXdSABHkh-', 'uNlQgFeSSJ30SJ9MnRm4_Q0qOU_MoLCzSDIPPeEYkNg_-0VlX7', 'OA7fxN0sPo7hSdplnEmJCvW-f7AiymibbyyNpyiFWI-VYRF8cj', 123, '', '2017-10-03 06:06:08'),
(20, 'p-dVIjtwqfDxWhOb1nzc6-0P_7MH-dV50e-KmFaPUt04W6BJlz', 'uNlQgFeSSJ30SJ9MnRm4_Q0qOU_MoLCzSDIPPeEYkNg_-0VlX7', 'yUmshqCI7mOezP6Uzmh_B6xoDDAr62E_2WrmTnaqA-BvDTUr_B', 0, '', '2017-10-03 06:06:08');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `_id` int(11) NOT NULL,
  `_name` varchar(100) NOT NULL,
  `_token` varchar(50) NOT NULL,
  `_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`_id`, `_name`, `_token`, `_created`) VALUES
(3, 'Mobile Dev Team - Full Stack', '5hang9uQziyZBqHfrKMFH_6FsmzdjBtPnVHpd0zsX8VY8-5lVO', '2017-09-27 10:10:20'),
(2, 'SSH', 'Gw4nDyrecl0g0CbZ8BOha_aHd199Sq59coRFQFL6QrEwPrSjSV', '2017-09-27 10:04:29'),
(6, 'CETD2', 'HErtwkVPadhhf0XmPYI_vMXDmEth0jEpy_jYzpPq8N_LTPbqTK', '2017-09-28 06:18:56'),
(1, 'CNSM', 'kVqtrN_8_isdhUXYdy015KOHNpSED3i6rEIOBEcYX', '2017-09-27 10:04:29'),
(9, '<deleted>Virtual Assistants</deleted>', 'lJgWXkUsrY5dGIHscUYDlEDDX3YKGkQu1CqqN2W4JBgOsn1fA4', '2017-09-30 22:38:58'),
(11, '<deleted>sample - department</deleted>', 'OiCDmOMtVIbKr8ZUXnNOHsfo8UH-slD_1cU4PcQD6lwCMpKmzm', '2017-09-30 23:05:25'),
(4, 'Web Dev Team', 'QPwNk0nZJNaR-IFKCU0uxW_seUNtGp7TlzexbWQMIC6qQdQS2Q', '2017-09-27 10:10:20'),
(5, 'Admin Office - Samatosa', 'zQju0nGNwPLch-vl0gpjZssIQW4BRvgAIyuLEUHJnTnz7muUBG', '2017-09-28 03:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `_id` int(11) NOT NULL,
  `_name` varchar(100) NOT NULL,
  `_token` varchar(50) NOT NULL,
  `_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`_id`, `_name`, `_token`, `_created`) VALUES
(6, 'sr developer', '-GEftmy0J8tqA__bpmby1eHaO4AkzRqJF0lyXAXzJZgYUaZsLF', '2017-09-27 10:08:23'),
(16, '<deleted>sample to delete</deleted>', '-W4gPzgTcUBbP7c4DfEuPv2wrWZ9HUPPXlFVUAx0Ov7cNSxhe9', '2017-09-30 23:04:21'),
(2, 'dean', '0mnqyG85LiDczQw4tn3zrwY-XmqfXubFU_t2OtZM50XFMd34hb', '2017-09-27 10:08:23'),
(12, 'system-admin', '1', '2017-09-29 06:49:55'),
(9, 'payroll officer', '28lnsmzQCQ_wgT2wDlKqoBwlOsmSzhK3WVCzyGssIrdi5xodWP', '2017-09-27 10:08:23'),
(1, 'faculty', '3bsUD5IH2r_Hy5gsRo-79bbfcPgYb7dQSswYpE7Wz1wQyuFh4l', '2017-09-27 10:08:23'),
(3, 'staff - 1', '9iRpwGTzGRybI0JedXJciPo1xl5c1MFHnPrYGGgZv4ubYeYCEU', '2017-09-27 10:08:23'),
(4, 'project manager', 'BSCvdmdgap43alF_r1VDGOj57uS_us_n-xGSm4oxOW5-1phlho', '2017-09-27 10:08:23'),
(7, 'HR', 'gEldG4M1nGadBevn18fDXemw-RjasP4a1u7CWJq5SSqJ2J72eH', '2017-09-27 10:08:23'),
(11, 'freelance developer 2', 'JhxawESHRyApOcyk_m5Uud34UuPmjoaIkFvA1wDY1EUiqi9zMb', '2017-09-28 06:27:26'),
(14, '<deleted>Jr Software Developer</deleted>', 'Ry69cFbtVkIi6A89XCgml3-JbB2eYuUIxaD13qQwlO4syHCm9t', '2017-09-30 22:41:36'),
(5, 'jr developer', 'UafcqzbHP7NcyWhABAnaqbiP-kMRN5-7M5I_op7hFDYk-Sr0pH', '2017-09-27 10:08:23'),
(10, 'chairman', 'YPXBl8kMyRQD1ZF2L8xmcrGqltOzDj6E1zm_1kKrBbZlbfhzN5', '2017-09-27 10:08:47'),
(8, 'Accountant', 'ZcDzXNWJwRm_E1GJNo2LXydqzAoBC5jBAabgppJZAV8x3fbjky', '2017-09-27 10:08:23');

-- --------------------------------------------------------

--
-- Table structure for table `scheduled_payslip`
--

CREATE TABLE `scheduled_payslip` (
  `_id` int(11) NOT NULL,
  `_basic_salary` double NOT NULL DEFAULT '0',
  `billing_schedule` varchar(100) NOT NULL,
  `billing_coverage` varchar(100) NOT NULL,
  `tax` double NOT NULL DEFAULT '0',
  `net_salary` double NOT NULL,
  `_employee` varchar(50) NOT NULL,
  `_token` varchar(50) NOT NULL,
  `_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheduled_payslip`
--

INSERT INTO `scheduled_payslip` (`_id`, `_basic_salary`, `billing_schedule`, `billing_coverage`, `tax`, `net_salary`, `_employee`, `_token`, `_created`) VALUES
(6, 15000, 'February Salary', '2017/10/02 - 2017/10/15', 1690, 12860, 'q5SSqJ2J72eHZcDzXNWJwRm_E1GJNo2LXydqzAoBC5jBAabgpp', 'p0JR7troUFIu24ClsGJHrX9JRPwb1NThQPQKBntM9rp5SsJXzq', '2017-10-02 07:33:24'),
(5, 12000, 'February Salary', '2017/10/02 - 2017/10/15', 1161.4, 10745.6, '4ioA0oojnZ6TF6O6Oq8nhZov4neRFKVe_D70w862fUzFDYKIEW', 'uNlQgFeSSJ30SJ9MnRm4_Q0qOU_MoLCzSDIPPeEYkNg_-0VlX7', '2017-10-02 07:33:24'),
(3, 0, 'January Salary', '2017/10/02 - 2017/10/03', 0, 0, 'K0uQTfPkHd7VG-DpMb3bsUD5IH2r_Hy5gsRo-79bbfcPgYb7dQ', 'aWRABiDvw5UagPJOHoI48jYfsy22_z5FzkGPxiTJLtj7yh3eMJ', '2017-10-02 07:31:01'),
(2, 0, 'January Salary', '2017/10/02 - 2017/10/03', 0, 0, 'q5SSqJ2J72eHZcDzXNWJwRm_E1GJNo2LXydqzAoBC5jBAabgpp', 'ayFlPm02dzKzZU9m9x8pod19M9ECR5mcLe_W4hUfUtV7Qxo0XF', '2017-10-02 07:31:01'),
(4, 0, 'January Salary', '2017/10/02 - 2017/10/03', 0, 0, 'UT6-3Hu0WA9iX8bcwueM6qSq1dS6LpIY1zJA0vdib0kuhjSp_R', 'JLMfoVnRFqYZCxkBSVRbvPmI5YcDMxeX6iM4brH1EHigwgtKrU', '2017-10-02 07:31:01'),
(1, 15000, 'January Salary', '2017/10/02 - 2017/10/03', 1713.4, 12953.6, '4ioA0oojnZ6TF6O6Oq8nhZov4neRFKVe_D70w862fUzFDYKIEW', 'RFKVe_D70w862fUzFDYKIEWjvKVyXUu7jAMhvLkKRVgI9_aUPu', '2017-10-02 07:31:01'),
(7, 0, 'NIS - january salary', '2017/10/02 - 2017/10/03', 0, 0, 'DJ0pHFl6IZbPGggR1DN2MEuNmqzTLyLHp7g2kKQiZtFihXtnMd', 'WbsBh7z5YVhPpDQABUWK0ON8CV77Tl9xLIDJJ3JwL0ahjFcRnB', '2017-10-02 08:41:39'),
(8, 0, 'NIS - january salary', '2017/10/02 - 2017/10/03', 0, 0, 'y4Lwqf0eIN5i41sPNQ3mxwo1yYoqxtyEKVRhRDJq6s9FdLD4pl', 'xyoVnfmcshJzZusAGd2mLjruX753su0G0KFpRKZTFPs9FP2RB5', '2017-10-02 08:41:39');

-- --------------------------------------------------------

--
-- Table structure for table `user_credible`
--

CREATE TABLE `user_credible` (
  `_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(500) NOT NULL,
  `_token` varchar(50) NOT NULL,
  `_active` tinyint(1) NOT NULL DEFAULT '1',
  `_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_credible`
--

INSERT INTO `user_credible` (`_id`, `email`, `password`, `_token`, `_active`, `_created`) VALUES
(4, 'admin@admin', '$2y$10$bJkhThk.s1SSVAd7H0TA1.JczGZatfhvw5QOeg6n50P2iHOQa0AAK', '1', 1, '2017-09-28 03:34:07'),
(9, 'test2@gmail.com', '$2y$10$xtSPtKJSov1VVEwYuZWUMuuXzIRM/vmf.sJs0BmvDBBbFHKYPKace', '6X5OQF2sOAeCl8Ktjk8K3CzEqYAVLQhMTchU8yGypP0r-urMNy', 1, '2017-09-28 07:59:34'),
(10, 'test3@gmail.com', '$2y$10$emEISc5.AbOmWLrTUFIjx.VmOKdZnWiYhn0OOyw/2zFwpkvf1vnPC', 'A-bOSnAkYXV_srvet3c2AymUU6QK_tY1YRyNpY_qq-6YBKfP1l', 1, '2017-09-28 08:01:27'),
(5, 'test@gmail.com', '$2y$10$fo2Sqb/JvpqnTIONVSkXJuKz/M4/LVQVrAx9GpOteFC6A78yXgWSG', 'ofl-SQJabzLnSzGrRpdJCcA27xuMRu8KMGihka9fwAq-O-km80', 1, '2017-09-28 07:39:19'),
(3, 'msu-gsc-payroll@email.com', '$2y$10$JnZIvuyzh7Y1.h8sLeUaZumt.OvAL2JRDNPJsiJXH7Nj82wfEqAmy', 't3ipoV4Vo3SabS5l-9NZyhma15PycWCafCJEVvC2osf-qP6Ado', 1, '2017-09-28 02:53:05'),
(31, 'admin2@asd', '$2y$10$aYuJzXNla5Bax8s8JurCyuITbcp3fcsraOWwiWo1oA0DeZHj2plpO', 'tzceDyEWdpsQZUv1TjXBYX7dNuYWwIP26WBq-JrCwuFsq4GoVY', 1, '2017-10-01 01:25:50'),
(1, 'adeanladia@gmail.com', '$2y$10$PBwbDTubJ8I7r8WhQW4Bxug2fP5.UorU8vu/Sr68fqBi9OeJEf6tC', 'ul1t-M_ReDy7TfcqauWjPGaV1GWNyQD1AZhgQrkzy4Lwqf0eIN', 1, '2017-09-27 10:39:21'),
(2, 'nis-payroll@email.com', '$2y$10$xGReQmC2KJhp528OWnsfE.gP7h5EiSkPbrlm7blPL4T0Muw7vnbwW', '_AcUcZlAY334cr4KpbCnzWml6JAu1aY9OoKbnYCXeZPel03qab', 1, '2017-09-28 02:53:05');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `_id` int(11) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `lastname` varchar(250) NOT NULL,
  `employee_id` varchar(50) NOT NULL,
  `_company` varchar(50) DEFAULT NULL,
  `_department` varchar(50) DEFAULT NULL,
  `_position` varchar(50) DEFAULT NULL,
  `_credibles` varchar(50) NOT NULL,
  `_token` varchar(50) NOT NULL,
  `_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`_id`, `firstname`, `lastname`, `employee_id`, `_company`, `_department`, `_position`, `_credibles`, `_token`, `_created`) VALUES
(39, 'John', 'Doe', '1', NULL, NULL, '1', '1', '1', '2017-09-29 06:50:27'),
(38, 'test2', '2222', '', 'vqZTS0coyiSw90iKMml0Z3mQSla82W7pC04D9lmeM6Pd13O33o', 'kVqtrN_8_isdhUXYdy015KOHNpSED3i6rEIOBEcYX', NULL, '6X5OQF2sOAeCl8Ktjk8K3CzEqYAVLQhMTchU8yGypP0r-urMNy', 'q5SSqJ2J72eHZcDzXNWJwRm_E1GJNo2LXydqzAoBC5jBAabgpp', '2017-09-28 08:19:35'),
(32, 'test4', '1111', '123123', 'vqZTS0coyiSw90iKMml0Z3mQSla82W7pC04D9lmeM6Pd13O33o', 'Gw4nDyrecl0g0CbZ8BOha_aHd199Sq59coRFQFL6QrEwPrSjSV', '-GEftmy0J8tqA__bpmby1eHaO4AkzRqJF0lyXAXzJZgYUaZsLF', 'A-bOSnAkYXV_srvet3c2AymUU6QK_tY1YRyNpY_qq-6YBKfP1l', 'K0uQTfPkHd7VG-DpMb3bsUD5IH2r_Hy5gsRo-79bbfcPgYb7dQ', '2017-09-28 08:12:19'),
(43, 'Adean', 'Ladia', '', NULL, NULL, NULL, 'ofl-SQJabzLnSzGrRpdJCcA27xuMRu8KMGihka9fwAq-O-km80', 'NDFhnHZvZx4n0fcJws3VVdSNb8lGOkIqJmGoqktpSRPPx-Wtnm', '2017-09-29 10:50:49'),
(24, 'msu-gsc-payroll', 'officer', '', 'vqZTS0coyiSw90iKMml0Z3mQSla82W7pC04D9lmeM6Pd13O33o', NULL, '28lnsmzQCQ_wgT2wDlKqoBwlOsmSzhK3WVCzyGssIrdi5xodWP', 't3ipoV4Vo3SabS5l-9NZyhma15PycWCafCJEVvC2osf-qP6Ado', '4ioA0oojnZ6TF6O6Oq8nhZov4neRFKVe_D70w862fUzFDYKIEW', '2017-09-28 03:18:22'),
(47, '', '', '', 'vqZTS0coyiSw90iKMml0Z3mQSla82W7pC04D9lmeM6Pd13O33o', 'Gw4nDyrecl0g0CbZ8BOha_aHd199Sq59coRFQFL6QrEwPrSjSV', '3bsUD5IH2r_Hy5gsRo-79bbfcPgYb7dQSswYpE7Wz1wQyuFh4l', 'tzceDyEWdpsQZUv1TjXBYX7dNuYWwIP26WBq-JrCwuFsq4GoVY', 'UT6-3Hu0WA9iX8bcwueM6qSq1dS6LpIY1zJA0vdib0kuhjSp_R', '2017-10-01 01:26:02'),
(11, 'Adean', 'Ladia', '', '8IrQcCgBPN7pjG6uzeQyUZmO8Pi_GfLikfitqh2JQjzMTO_QqK', '5hang9uQziyZBqHfrKMFH_6FsmzdjBtPnVHpd0zsX8VY8-5lVO', 'UafcqzbHP7NcyWhABAnaqbiP-kMRN5-7M5I_op7hFDYk-Sr0pH', 'ul1t-M_ReDy7TfcqauWjPGaV1GWNyQD1AZhgQrkzy4Lwqf0eIN', 'DJ0pHFl6IZbPGggR1DN2MEuNmqzTLyLHp7g2kKQiZtFihXtnMd', '2017-09-28 02:49:58'),
(22, 'nis-payroll', 'officer', '', '8IrQcCgBPN7pjG6uzeQyUZmO8Pi_GfLikfitqh2JQjzMTO_QqK', NULL, '28lnsmzQCQ_wgT2wDlKqoBwlOsmSzhK3WVCzyGssIrdi5xodWP', '_AcUcZlAY334cr4KpbCnzWml6JAu1aY9OoKbnYCXeZPel03qab', 'y4Lwqf0eIN5i41sPNQ3mxwo1yYoqxtyEKVRhRDJq6s9FdLD4pl', '2017-09-28 03:17:28');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_deductables`
--
CREATE TABLE `view_deductables` (
`deduction` varchar(100)
,`_token` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_incentives`
--
CREATE TABLE `view_incentives` (
`incentive` varchar(100)
,`_token` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_payrolls`
--
CREATE TABLE `view_payrolls` (
`payslip_id` varchar(50)
,`_login` varchar(50)
,`_user` varchar(50)
,`email` varchar(50)
,`password` varchar(500)
,`firstname` varchar(250)
,`lastname` varchar(250)
,`employee_id` varchar(50)
,`_company` varchar(50)
,`company` varchar(100)
,`_department` varchar(50)
,`department` varchar(100)
,`_position` varchar(50)
,`position` varchar(100)
,`_active` tinyint(1)
,`_log` timestamp
,`billing` varchar(100)
,`coverage` varchar(100)
,`gross_salary` double
,`net` double
,`tax` double
,`payslip_generated_on` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_payslip_factors`
--
CREATE TABLE `view_payslip_factors` (
`employee` varchar(50)
,`basic` double
,`payslip_id` varchar(50)
,`cmptbl_psched_id` varchar(50)
,`factor_id` varchar(50)
,`amt` double
,`factor` varchar(100)
,`type` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_users`
--
CREATE TABLE `view_users` (
`_login` varchar(50)
,`_user` varchar(50)
,`email` varchar(50)
,`password` varchar(500)
,`firstname` varchar(250)
,`lastname` varchar(250)
,`employee_id` varchar(50)
,`_company` varchar(50)
,`company` varchar(100)
,`_department` varchar(50)
,`department` varchar(100)
,`_position` varchar(50)
,`position` varchar(100)
,`_active` tinyint(1)
,`_log` timestamp
);

-- --------------------------------------------------------

--
-- Structure for view `view_deductables`
--
DROP TABLE IF EXISTS `view_deductables`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_deductables`  AS  select `a`.`_name` AS `deduction`,`a`.`_token` AS `_token` from `computable_factors` `a` where (`a`.`_is_dedctn_not_incntv` = 1) ;

-- --------------------------------------------------------

--
-- Structure for view `view_incentives`
--
DROP TABLE IF EXISTS `view_incentives`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_incentives`  AS  select `a`.`_name` AS `incentive`,`a`.`_token` AS `_token` from `computable_factors` `a` where (`a`.`_is_dedctn_not_incntv` = 0) ;

-- --------------------------------------------------------

--
-- Structure for view `view_payrolls`
--
DROP TABLE IF EXISTS `view_payrolls`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_payrolls`  AS  select distinct `b`.`_token` AS `payslip_id`,`a`.`_login` AS `_login`,`a`.`_user` AS `_user`,`a`.`email` AS `email`,`a`.`password` AS `password`,`a`.`firstname` AS `firstname`,`a`.`lastname` AS `lastname`,`a`.`employee_id` AS `employee_id`,`a`.`_company` AS `_company`,`a`.`company` AS `company`,`a`.`_department` AS `_department`,`a`.`department` AS `department`,`a`.`_position` AS `_position`,`a`.`position` AS `position`,`a`.`_active` AS `_active`,`a`.`_log` AS `_log`,`b`.`billing_schedule` AS `billing`,`b`.`billing_coverage` AS `coverage`,`b`.`_basic_salary` AS `gross_salary`,`b`.`net_salary` AS `net`,`b`.`tax` AS `tax`,`b`.`_created` AS `payslip_generated_on` from (`view_users` `a` join `scheduled_payslip` `b`) where (`a`.`_user` = `b`.`_employee`) ;

-- --------------------------------------------------------

--
-- Structure for view `view_payslip_factors`
--
DROP TABLE IF EXISTS `view_payslip_factors`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_payslip_factors`  AS  select distinct `a`.`_employee` AS `employee`,`a`.`_basic_salary` AS `basic`,`a`.`_token` AS `payslip_id`,`b`.`_token` AS `cmptbl_psched_id`,`b`.`_factor` AS `factor_id`,`b`.`amount` AS `amt`,`c`.`_name` AS `factor`,`c`.`_is_dedctn_not_incntv` AS `type` from ((`scheduled_payslip` `a` join `computable_per_schedule` `b`) join `computable_factors` `c`) where ((`a`.`_token` = `b`.`_payslip`) and (`b`.`_factor` = `c`.`_token`)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_users`
--
DROP TABLE IF EXISTS `view_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_users`  AS  select distinct `a`.`_token` AS `_login`,`b`.`_token` AS `_user`,`a`.`email` AS `email`,`a`.`password` AS `password`,`b`.`firstname` AS `firstname`,`b`.`lastname` AS `lastname`,`b`.`employee_id` AS `employee_id`,`b`.`_company` AS `_company`,`c`.`_name` AS `company`,`b`.`_department` AS `_department`,`d`.`_name` AS `department`,`b`.`_position` AS `_position`,`e`.`_name` AS `position`,`a`.`_active` AS `_active`,`a`.`_created` AS `_log` from ((((`user_credible` `a` left join `user_details` `b` on((`a`.`_token` = `b`.`_credibles`))) left join `companies` `c` on((`b`.`_company` = `c`.`_token`))) left join `departments` `d` on((`b`.`_department` = `d`.`_token`))) left join `positions` `e` on((`b`.`_position` = `e`.`_token`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`_token`) USING BTREE,
  ADD KEY `_id` (`_id`);

--
-- Indexes for table `computable_factors`
--
ALTER TABLE `computable_factors`
  ADD PRIMARY KEY (`_token`),
  ADD UNIQUE KEY `_name` (`_name`),
  ADD KEY `_id` (`_id`);

--
-- Indexes for table `computable_per_schedule`
--
ALTER TABLE `computable_per_schedule`
  ADD PRIMARY KEY (`_payslip`,`_factor`) USING BTREE,
  ADD UNIQUE KEY `_token` (`_token`),
  ADD KEY `_factor` (`_factor`),
  ADD KEY `_id` (`_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`_token`) USING BTREE,
  ADD KEY `_id` (`_id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`_token`) USING BTREE,
  ADD KEY `_id` (`_id`);

--
-- Indexes for table `scheduled_payslip`
--
ALTER TABLE `scheduled_payslip`
  ADD PRIMARY KEY (`billing_schedule`,`billing_coverage`,`_token`) USING BTREE,
  ADD UNIQUE KEY `_token` (`_token`) USING BTREE,
  ADD KEY `_employee` (`_employee`),
  ADD KEY `_id` (`_id`);

--
-- Indexes for table `user_credible`
--
ALTER TABLE `user_credible`
  ADD PRIMARY KEY (`_token`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `_id` (`_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`_credibles`) USING BTREE,
  ADD UNIQUE KEY `_token` (`_token`) USING BTREE,
  ADD KEY `_position` (`_position`),
  ADD KEY `_company` (`_company`),
  ADD KEY `_department` (`_department`) USING BTREE,
  ADD KEY `_id` (`_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `computable_factors`
--
ALTER TABLE `computable_factors`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `computable_per_schedule`
--
ALTER TABLE `computable_per_schedule`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `scheduled_payslip`
--
ALTER TABLE `scheduled_payslip`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_credible`
--
ALTER TABLE `user_credible`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `computable_per_schedule`
--
ALTER TABLE `computable_per_schedule`
  ADD CONSTRAINT `computable_per_schedule_ibfk_1` FOREIGN KEY (`_factor`) REFERENCES `computable_factors` (`_token`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `computable_per_schedule_ibfk_2` FOREIGN KEY (`_payslip`) REFERENCES `scheduled_payslip` (`_token`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `scheduled_payslip`
--
ALTER TABLE `scheduled_payslip`
  ADD CONSTRAINT `scheduled_payslip_ibfk_1` FOREIGN KEY (`_employee`) REFERENCES `user_details` (`_token`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_ibfk_1` FOREIGN KEY (`_credibles`) REFERENCES `user_credible` (`_token`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_details_ibfk_2` FOREIGN KEY (`_position`) REFERENCES `positions` (`_token`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_details_ibfk_3` FOREIGN KEY (`_company`) REFERENCES `companies` (`_token`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_details_ibfk_4` FOREIGN KEY (`_department`) REFERENCES `departments` (`_token`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
