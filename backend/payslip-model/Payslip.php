<?php 

class Payslip {

	public function __construct(){}

	public function insert($db, $schedule, $coverage, $employee_token){
		if($schedule != null && $coverage != null && count($employee_token) > 0){
			if(trim($schedule) != "" && trim($coverage) != ""){
				$schedule = mysqli_real_escape_string($db, $schedule);
				$coverage = mysqli_real_escape_string($db, $coverage);

				$queryString = "INSERT INTO scheduled_payslip (billing_schedule, billing_coverage, _employee, _token, tax, _basic_salary) VALUES ";

				for ($i=0; $i < count($employee_token); $i++) { 
					$employee = mysqli_real_escape_string($db, $employee_token[$i]["_employee"]["_user"]);
					$prev_salary = mysqli_real_escape_string($db, $employee_token[$i]["_employee"]["prev_salary"]);
					$prev_tax = mysqli_real_escape_string($db, $employee_token[$i]["_employee"]["prev_tax"]);
					$token = mysqli_real_escape_string($db, $employee_token[$i]["_token"]);

					$queryString = $queryString."('".$schedule."', '".$coverage."', '".$employee."', '".$token."', ".$prev_tax.", ".$prev_salary.")";

					if($i != (count($employee_token)-1)){ $queryString = $queryString.", "; }
				}

				$query = mysqli_query($db, $queryString);
				if($query){ return $employee_token; }
			}
		}

		return null;
	}

	public function find_payroll_by_company($db, $company){
		if($company != null && trim($company) != ""){
			$search = mysqli_real_escape_string($db, $company);
			$queryString = "SELECT * FROM view_payrolls a WHERE a._company='".$search."' ORDER BY a._log ASC";
	
			$query = mysqli_query($db, $queryString);
			$arr = array();
			if($query){
				while ($row = mysqli_fetch_array($query)) { 
					array_push($arr, 
						array(
							"payslip_id" => $row["payslip_id"], 
							"_login" => $row["_login"],
							"_user" => $row["_user"],
							"email" => $row["email"],
							"password" => $row["password"],
							"firstname" => $row["firstname"],
							"lastname" => $row["lastname"],
							"employee_id" => $row["employee_id"],
							"_company" => $row["_company"],
							"_department" => $row["_department"],
							"department" => $row["department"],
							"_position" => $row["_position"],
							"position" => $row["position"],
							"_active" => $row["_active"],
							"_log" => $row["_log"],
							"billing" => $row["billing"],
							"coverage" => $row["coverage"],
							"gross_salary" => $row["gross_salary"],
							"tax" => $row["tax"],
							"net" => $row["net"],
							"payslip_generated_on" => $row["payslip_generated_on"],
						)
					);
				}
			}

			return $arr;
		}

		return null;
	}

	public function find_payroll_by_company_withPrev($db, $company){
		if($company != null && trim($company) != ""){
			$search = mysqli_real_escape_string($db, $company);
			$queryString = "SELECT * FROM view_payrolls c LEFT JOIN (SELECT b._id as prev_id, b._employee as prev_employee, b.tax as prev_tax, b._basic_salary as prev_salary, b.billing_schedule, b.billing_coverage FROM scheduled_payslip b WHERE b._id NOT IN (SELECT max(a._id) FROM scheduled_payslip a GROUP BY a._employee)) as d ON d.prev_employee = c._user WHERE c._company='".$search."' GROUP BY c.payslip_id ORDER BY c._log DESC";
	
			$query = mysqli_query($db, $queryString);
			$arr = array();
			if($query){
				while ($row = mysqli_fetch_array($query)) { 
					array_push($arr, 
						array(
							"payslip_id" => $row["payslip_id"], 
							"_login" => $row["_login"],
							"_user" => $row["_user"],
							"email" => $row["email"],
							"password" => $row["password"],
							"firstname" => $row["firstname"],
							"lastname" => $row["lastname"],
							"employee_id" => $row["employee_id"],
							"_company" => $row["_company"],
							"_department" => $row["_department"],
							"department" => $row["department"],
							"_position" => $row["_position"],
							"position" => $row["position"],
							"_active" => $row["_active"],
							"_log" => $row["_log"],
							"billing" => $row["billing"],
							"coverage" => $row["coverage"],
							"gross_salary" => $row["gross_salary"],
							"tax" => $row["tax"],
							"net" => $row["net"],
							"payslip_generated_on" => $row["payslip_generated_on"],
							"prev_tax" => $row["prev_tax"],
							"prev_salary" => $row["prev_salary"],
							"last_billing" => $row["billing_schedule"],
							"last_coverage" => $row["billing_coverage"],
						)
					);
				}
			}

			return $arr;
		}

		return null;
	}

	public function find_payroll_by_company_filters($db, $company){
		if($company != null && trim($company) != ""){
			$search = mysqli_real_escape_string($db, $company);
			$queryString = "SELECT * FROM view_payrolls a WHERE a._company='".$search."' GROUP BY billing, coverage ORDER BY payslip_generated_on DESC";
	
			$query = mysqli_query($db, $queryString);
			$arr = array();
			if($query){
				while ($row = mysqli_fetch_array($query)) { 
					array_push($arr, 
						array(
							"billing" => $row["billing"],
							"coverage" => $row["coverage"],
						)
					);
				}
			}

			return $arr;
		}

		return null;
	}

	public function update_basicSalary($db, $basic, $payslip){
		if($basic != null && $payslip != null){
			if(trim($basic) != "" && trim($payslip) != ""){
				$basic = mysqli_real_escape_string($db, $basic);
				$payslip = mysqli_real_escape_string($db, $payslip);

				$queryString = "UPDATE scheduled_payslip SET _basic_salary=".$basic." WHERE _token='".$payslip."' LIMIT 1";
				$query = mysqli_query($db, $queryString);
				if($query){ return $basic; }
			}
		}

		return null;
	}

	public function update_Tax($db, $tax, $net, $payslip){
		if($tax != null && $payslip != null){
			if(trim($tax) != "" && trim($payslip) != ""){
				$tax = mysqli_real_escape_string($db, $tax);
				$net = mysqli_real_escape_string($db, $net);
				$payslip = mysqli_real_escape_string($db, $payslip);

				$queryString = "UPDATE scheduled_payslip SET tax=".$tax.", net_salary=".$net." WHERE _token='".$payslip."' LIMIT 1";
				$query = mysqli_query($db, $queryString);
				if($query){ return $tax; }
			}
		}

		return null;
	}

	public function upsertComputable_perSchedule($db, $amt, $factor, $payslip, $token){
		if($amt != null && $factor != null && $payslip != null && $token != null){
			if(trim($amt) != "" && trim($factor) != "" && trim($payslip) != "" && trim($token) != ""){
				$amt = mysqli_real_escape_string($db, $amt);
				$factor = mysqli_real_escape_string($db, $factor);
				$payslip = mysqli_real_escape_string($db, $payslip);
				$token = mysqli_real_escape_string($db, $token);

				$queryString = "INSERT INTO computable_per_schedule (amount, _factor, _payslip, _token) VALUES (".$amt.", '".$factor."', '".$payslip."', '".$token."') ON DUPLICATE KEY UPDATE amount = '".$amt."'";
				$query = mysqli_query($db, $queryString);
				if($query){ return $token; }
			}
		}

		return null;
	}

	public function getExistingComputables($db, $payslip_id){
		if($payslip_id != null && trim($payslip_id) != ""){
			$search = mysqli_real_escape_string($db, $payslip_id);
			$queryString = "SELECT * FROM view_payslip_factors a WHERE a.payslip_id='".$search."'";
	
			$query = mysqli_query($db, $queryString);
			$arr = array();
			if($query){
				while ($row = mysqli_fetch_array($query)) { 
					array_push($arr, 
						array(
							"employee" => $row["employee"], 
							"basic" => $row["basic"],
							"payslip_id" => $row["payslip_id"],
							"token" => $row["cmptbl_psched_id"],
							"factor_id" => $row["factor_id"],
							"amt" => $row["amt"],
							"factor" => $row["factor"],
							"type" => $row["type"],
						)
					);
				}
			}

			return $arr;
		}
	}

	public function getMyPayslips($db, $employee){
		if($employee != null && trim($employee) != ""){
			$search = mysqli_real_escape_string($db, $employee);
			$queryString = "SELECT * FROM scheduled_payslip a WHERE a._employee='".$search."' ORDER BY _created DESC";
	
			$query = mysqli_query($db, $queryString);
			$arr = array();
			if($query){
				while ($row = mysqli_fetch_array($query)) { 
					array_push($arr, 
						array(
							"_basic_salary" => $row["_basic_salary"], 
							"billing_schedule" => $row["billing_schedule"],
							"billing_coverage" => $row["billing_coverage"],
							"tax" => $row["tax"],
							"net" => $row["net_salary"],
							"_employee" => $row["_employee"],
							"_token" => $row["_token"],
						)
					);
				}
			}

			return $arr;
		}
	}

	public function update(){
		// fail silently
		return false;
	}

	public function delete($token){
		// fail silently
		return false;
	}
}

?>