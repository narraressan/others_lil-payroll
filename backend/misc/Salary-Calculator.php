<?php 

// computations based on BIR policy - 2017

class SalaryCalculator {

	private $basicSalary = 0;
	private $totalIncentives = 0;

	private $countMonths = 0; // default - 12 months - unless specified
	private $yrlyPersonalExemption = 0; // default as of this BIR policy is P 50,000

	
	public function __construct($basicSalary, $totalIncentives, $countMonths, $yrlyPersonalExemption = 50000){
		$this->basicSalary = $basicSalary;
		$this->totalIncentives = $totalIncentives;
		$this->countMonths = $countMonths;
		$this->yrlyPersonalExemption = $yrlyPersonalExemption;
	}

	
	public function getGrossIncome(){
		return ($this->basicSalary + $this->totalIncentives);
	}


	public function getTaxableIncome($gross_taxableIncome){
		// taxable income on remaining "count" months
		$yrly_taxableIncome = ($gross_taxableIncome * $this->countMonths) - $this->yrlyPersonalExemption;
		$net_taxableIncome = $yrly_taxableIncome / $this->countMonths;

		return $net_taxableIncome;
	}


	public function taxTable($net_taxableIncome){
		$rate = 0;

		if($net_taxableIncome > 0 && $net_taxableIncome <= 10000){ $rate = "return (0.05 * ".$net_taxableIncome.");"; }
		else if($net_taxableIncome > 10000 && $net_taxableIncome <= 30000){ $rate = "return (500 + (0.10 * (".$net_taxableIncome."-10000)));"; }
		else if($net_taxableIncome > 30000 && $net_taxableIncome <= 70000){ $rate = "return (2500 + (0.15 * (".$net_taxableIncome."-30000)));"; }
		else if($net_taxableIncome > 70000 && $net_taxableIncome <= 140000){ $rate = "return (8500 + (0.20 * (".$net_taxableIncome."-70000)));"; }
		else if($net_taxableIncome > 140000 && $net_taxableIncome <= 250000){ $rate = "return (22500 + (0.25 * (".$net_taxableIncome."-140000)));"; }
		else if($net_taxableIncome > 250000 && $net_taxableIncome <= 500000){ $rate = "return (50000 + (0.30 * (".$net_taxableIncome."-250000)));"; }
		// I cannot compute 500000 salary!
		else if($net_taxableIncome > 500000){ $rate = null; }

		return $rate;
	}


	// this function is not sure to be unique
	public function sampleEval(){
		return eval("return (1+1);");
	}


}

?>