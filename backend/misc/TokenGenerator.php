<?php 

class Token {
	private $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-";
	private $tokenLength = 0;

	public function __construct($len = 50){
		$this->tokenLength = $len;
	}

	// this function is not sure to be unique
	public function createToken(){
		$randstring = "";
		$len = strlen($this->characters);
		for ($i = 0; $i < $this->tokenLength; $i++) {
			$randstring .= $this->characters[rand(0, $len - 1)];
		}

		return $randstring;
	}
}

?>