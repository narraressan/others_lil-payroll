<?php 

class ConnectDB {
	private $ip = "";
	private $user = "";
	private $pass = "";
	private $dbname = "";

	public function __construct($ip="localhost", $user="root", $pass="", $dbname="lilpayroll"){
		$this->ip = $ip;
		$this->user = $user;
		$this->pass = $pass;
		$this->dbname = $dbname;
	}

	// open db connection
	public function openDB(){
		$openDB = mysqli_connect($this->ip, $this->user, $this->pass, $this->dbname);
		if(mysqli_connect_errno()){ return null; }
		return $openDB;
	}

	// close db connection
	public function closeDB($openDB){ mysqli_close($openDB);}
}

?>