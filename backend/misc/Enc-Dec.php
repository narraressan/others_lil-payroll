<?php 

class EncDec {
	private $secretText = null;

	public function __construct($txt = null){
		$this->secretText = $txt;
	}

	public function getSecret(){
		if($this->secretText != null){ return password_hash($this->secretText, PASSWORD_DEFAULT); }
		else{ return null; }
	}

	public function checkSecret($secret, $toCheck = ""){
		return password_verify($toCheck, $secret);
	}
}

?>