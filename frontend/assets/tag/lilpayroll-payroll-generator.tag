
<!-- Notes -->
<!-- create payslips only for employees on same company -->

<payrollgenerator-lilpayroll>
	<form class="payroll-generator" onsubmit={ createPayRoll }>
		<div class="row">
			<!-- create new billing schedule -->
			<div class="col-6 form-group">
				<label>Billing Schedule</label>
				<input type="text" class="form-control" placeholder="Billing Alias (e.g. 'Payslip for September 1 - 15')" name="billing_schedule" required>
			</div>
			<div class="col form-group">
				<label>Start Date</label>
				<input type="date" class="form-control" required name="billing_coverage_start">
			</div>
			<div class="col form-group">
				<label>End Date</label>
				<input type="date" class="form-control" required name="billing_coverage_end">
			</div>
		</div>

		<div class="row">
			<div class="col-12 form-inline">
				<p style="width: 100%;" class="text-right">
					<button class="btn btn-warning" onclick={ main }><span class="fa fa-refresh"></span></button>
					<select class="form-control" id="departmentFiter" onchange={ reFilterTable }>
						<option>All</option>
						<option each={ nth in departmentList } value={ nth }>{ nth }</option>
					</select>
					<select class="form-control" id="positionFilter" onchange={ reFilterTable }>
						<option>All</option>
						<option each={ nth in positionList } value={ nth }>{ nth }</option>
					</select>
				</p>
			</div>

			<div class="col-12">
				<table class="table table-striped table-hover table-bordered" id="employeesTable">
					<thead class="thead-inverse">
						<tr>
							<th>
								<center>
									<input type="checkbox" class="form-check" id="checkAll" onclick={ checkAll }>
								</center>
							</th>
							<th>Name</th>
							<th>Department</th>
							<th>Position</th>
							<th>Latest Billing</th>
							<th>Basic Salary</th>
						</tr>
					</thead>
					<tbody>
						<tr each={ compnyEmployees.filter(showFilter) }>
							<td>
								<center>
									<input type="checkbox" class="form-check" name={ _user } value={ _login }>
								</center>
							</td>
							<td>
								<span class="surname" if={ lastname.trim() != "" }><b>{ lastname }</b>, </span>
								{ firstname }
								<br if={ firstname.trim() != "" }>
								<small if={ email.trim() != "" }>- { email }</small>
							</td>
							<td>{ department }</td>
							<td>{ position }</td>
							<td>{ last_billing }<br><small>{ last_coverage }</small></td>
							<td>{ prev_salary }</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="row">
			<p class="col text-right">
				<button class="btn btn-primary" type="submit"><span class="fa fa-credit-card-alt"></span>&nbsp;&nbsp;Create Payroll</button>
			</p>
		</div>
	</form>


	<style type="text/css">
		.payroll-generator { margin-top: 20px; }
		.inline-label { padding-top: 5px; }
		.surname { text-transform: uppercase; }
	</style>


	<script>
		this.departmentList = [];
		this.positionList = [];
		this.compnyEmployees = [];

		extractDepartments(arr){
			var temp = [];
			for (var i = 0; i < arr.length; i++) {
				if(!temp.includes(arr[i]["department"]) && arr[i]["department"] != null){ temp.push(arr[i]["department"]); }
			}

			return temp;
		}

		extractPositions(arr){
			var temp = [];
			for (var i = 0; i < arr.length; i++) {
				if(!temp.includes(arr[i]["position"]) && arr[i]["position"] != null){ temp.push(arr[i]["position"]); }
			}

			return temp;
		}

		laodUsers(company){
			var self = this;
			$.ajax({
				method: "GET",
				url: host + "/get-users-by-company?company=" + company,
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, securing available users failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						self.toast("Employees retrieved");
						self.compnyEmployees = data["data"];
						self.departmentList = self.extractDepartments(self.compnyEmployees);
						self.positionList = self.extractPositions(self.compnyEmployees);
						self.update();
					}
					else{ self.toast("Sorry, securing available users failed"); }
				},
			})
		}

		showFilter(item){
			var dept = $("#departmentFiter")[0].value;
			var postn = $("#positionFilter")[0].value;

			if(dept == "All" && postn == "All"){ return item; }
			else{
				if(dept == item["department"] || postn == item["position"]){ return item; }
			}
		}

		reFilterTable(){ 
			$("#employeesTable").find("[type='checkbox']").prop("checked", false);
			this.update(); 
		}

		createPayRoll(){
			var formData = this.serializeForm(event);

			var payrollData = {
				billing_schedule: formData["billing_schedule"],
				billing_coverage: formData["billing_coverage_start"].replace(/\-/g, "/") + " - " + formData["billing_coverage_end"].replace(/\-/g, "/"),
				employees: [],
			};

			// assemble users
			for(var key in formData){
				if(!["billing_schedule", "billing_coverage_start", "billing_coverage_end"].includes(key)){
					for (var i = 0; i < this.compnyEmployees.length; i++) {
						if(this.compnyEmployees[i]["_user"] == key){
							if(this.compnyEmployees[i]["prev_salary"] == null){ this.compnyEmployees[i]["prev_salary"] = 0; }
							if(this.compnyEmployees[i]["prev_tax"] == null){ this.compnyEmployees[i]["prev_tax"] = 0; }

							payrollData["employees"].push(this.compnyEmployees[i]);
						}
					}
				}
			}

			if(payrollData["employees"].length > 0){
				// ajax!
				var self = this;
				$.ajax({
					method: "POST",
					url: host + "/create-payroll",
					data: payrollData,
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, creating payroll failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("Payroll created");
							self.changeLocation("view-payroll");
							self.update();
						}
						else{ self.toast("Sorry, creating payroll failed"); }
					},
				})

				event.target.reset();
			}

			event.preventDefault();
		}

		checkAll(){
			var set = $(event.target).is(":checked");
			$(event.target).closest("table").find("tbody").find("[type='checkbox']").prop("checked", set);
		}

		main(){
			this.initTag("payroll-generator");

			try{
				// the payroll officer should always have a company!
				var company = this.getMemory("usrSession")["_company"];
				this.laodUsers(company);
			}
			catch(err){ console.log("error in filtering employees by company!"); }
		}
		this.main();
	</script>
</payrollgenerator-lilpayroll>