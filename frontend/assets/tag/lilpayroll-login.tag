<login-lilpayroll>
	<div class="card">
		<div class="card-body">
			<center>
				<h4 class="card-title">Login</h4>
			</center>
			<br>

			<form onsubmit={ loginEmployee }>
				<div class="form-group">
					<label class="control-label">Email</label>
					<input class="form-control" type="email" placeholder="johndoe@gmail.com" name="email" required>
				</div>

				<div class="form-group">
					<label class="control-label">Password</label>
					<input class="form-control" type="password" placeholder="*********" name="password" required>
				</div>

				<button class="btn btn-primary" type="submit">Login</button>
			</form>
		</div>
	</div>


	<script>
		
		loginEmployee(event){
			var form = event;

			// ajax
			var data = this.serializeForm(form);
			$.ajax({
				method: "POST",
				url: host + "/user-login",
				data: data,
				// callback functions
					riot: this,
				error: function (xhr, status, err) { this.riot.toast("Sorry, login failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){
						this.riot.inMemory("usrSession", data["data"]);
						this.riot.changeLocation("#dashboard");
						this.riot.toast("Welcome back " + data["data"]["firstname"]);
					}
					else{ this.riot.toast("Sorry, login failed"); }
				},
			})

			form.preventDefault();
			form.target.reset();
		}

		main(){
			this.initTag("login");
		}
		this.main();

	</script>
</login-lilpayroll>