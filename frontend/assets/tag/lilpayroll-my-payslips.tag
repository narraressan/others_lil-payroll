<mypayslips-lilpayroll>
	<div class="mypayslips">
		<div class="row">
			<div class="col-12">
				<button class="btn btn-warning pull-right" onclick={ main }><span class="fa fa-refresh"></span></button>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<br>
				<table class="table table-striped table-hover table-bordered">
					<thead class="thead-inverse">
						<tr>
							<th>Schedule</th>
							<th>Coverage</th>
							<th>Net Salary</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr each={ nth in mypayslips }>
							<td>{ nth["billing_schedule"] }</td>
							<td>{ nth["billing_coverage"] }</td>
							<td>{ nth["net"] }</td>
							<td>
								<center>
									<div class="btn-group btn-group-sm">
										<button class="btn btn-primary" onclick={ stagePayslip } data-toggle="modal" data-target="#viewPayslip">View Payslip</button>
										<a href="./pdf-printable/payslip.html?payslip={ JSON.stringify(nth) }&user={ JSON.stringify(getMemory('usrSession')) }" target="_blank" class="btn btn-success"><span class="fa fa-download"></span>&nbsp;&nbsp;Download</a>
									</div>
								</center>
							</td>
						</tr>
					</tbody>
				</table>

				<div class="modal fade" id="viewPayslip">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<h4 class="text-center">{ getMemory("usrSession")["company"] }</h4><br>

								<table class="table table-hover table-sm">
									<tbody>
										<tr>
											<td>Employee Name:</td>
											<td><b>{ getMemory("usrSession")["firstname"] } { getMemory("usrSession")["lastname"] }</b></td>
										</tr>
										<tr>
											<td>Designated Position:</td>
											<td><b>{ getMemory("usrSession")["position"] }</b></td>
										</tr>
										<tr>
											<td>Department:</td>
											<td><b>{ getMemory("usrSession")["department"] }</b></td>
										</tr>
										<tr>
											<td>Billing:</td>
											<td>
												{ stageThisPayslip["billing_schedule"] } - { stageThisPayslip["billing_coverage"] }
											</td>
										</tr>
									</tbody>
								</table>
								<br>

								<table class="table table-hover table-sm">
									<thead class="thead-inverse">
										<tr>
											<th>Incentives</th>
											<th>Amount</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Basic Salary</td>
											<td>{ stageThisPayslip["_basic_salary"] }</td>
										</tr>
										<tr each={ incentives }>
											<td>{ factor }</td>
											<td>{ amt }</td>
										</tr>
									</tbody>
								</table>
								<br>

								<table class="table table-hover table-sm">
									<thead class="thead-inverse">
										<tr>
											<th>Deductions</th>
											<th>Amount</th>
										</tr>
									</thead>
									<tbody>
										<tr each={ deductions }>
											<td>{ factor }</td>
											<td>{ amt }</td>
										</tr>
										<tr>
											<td>Tax</td>
											<td>{ stageThisPayslip["tax"] }</td>
										</tr>
									</tbody>
								</table>
								<br>

								<table class="table table-hover table-sm">
									<tbody>
										<tr>
											<td>Net Salary:</td>
											<td><b>{ stageThisPayslip["net"] }</b></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<style type="text/css">
		.mypayslips { margin-top: 20px; }
	</style>


	<script>
		this.mypayslips = [];

		this.existingFactors = [];
		this.incentives = [];
		this.deductions = [];

		this.stageThisPayslip = {};

		loadMyPayslips(){
			var self = this;
			$.ajax({
				method: "GET",
				url: host + "/get-my-payslips?employee=" + this.getMemory("usrSession")["_user"],
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, securing available payslips failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						self.toast("Payslips retrieved");
						self.mypayslips = data["data"];
						self.update();
					}
					else{ self.toast("Sorry, securing available payslips failed"); }
				},
			})
		}
		
		stagePayslip(item){
			this.stageThisPayslip = item["item"]["nth"];
			this.update();

			this.loadExistingFactors();
		}

		loadExistingFactors(){
			var self = this;
			$.ajax({
				method: "GET",
				url: host + "/get-payslip-factors?payslip=" + this.stageThisPayslip["_token"],
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, securing available factors failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						self.toast("Existing Factors retrieved");
						self.existingFactors = data["data"];

						self.incentives = [];
						self.deductions = [];

						for (var i = 0; i < self.existingFactors.length; i++) {
							if(self.existingFactors[i]["type"] == 1){ self.deductions.push(self.existingFactors[i]); }
							if(self.existingFactors[i]["type"] == 0){ self.incentives.push(self.existingFactors[i]); }
						}

						self.update();
					}
					else{ self.toast("Sorry, securing available factors failed"); }
				},
			})
		}

		main(){
			this.initTag("my-payslips");

			this.loadMyPayslips();
		}
		this.main();
	</script>
</mypayslips-lilpayroll>