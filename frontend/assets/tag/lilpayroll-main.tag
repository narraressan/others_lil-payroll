
<main-lilpayroll>
	<div id="main">
		<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
			<div class="container">
				<span class="fa fa-caret-down fa-2x navbar-toggler navbar-toggler-right" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation"></span>
				<a class="navbar-brand" href="#login">
					<img src="https://2.bp.blogspot.com/-QLJ0yfBcivY/WQpelCqcyNI/AAAAAAAAqr4/37-_VDUsKiQGKGzsGk63VZm_UJ1dRGIRQCLcB/s1600/MSU_-_Gensan_logo.png" class="img-responsive" width="60px;">
					<b>LiL</b> - Payroll
				</a>

				<div class="collapse navbar-collapse" id="navbarMenu">
					<ul class="navbar-nav mr-auto"></ul>
					<form class="form-inline">
						<ul class="navbar-nav mr-auto" if={ page.name != "login" }>
							<li class="nav-item { 'active': active }" each={ routeablePages } if={ name != "login" && pageAccessible(positionFiltered) } >
								<a class="nav-link" href="#{ name }">{ name }</a>
							</li>
							<li><a class="nav-link" href="javascript:" onclick={ logoutSession }>- Logout -</a></li>
						</ul>
					</form>
				</div>
			</div>
		</nav>

		<div class="container">
			<!-- login -->
			<div class="row justify-content-md-center login-lilpayroll" if={ page.name == "login" }>
				<div class="col col-lg-5 align-self-center">
					<login-lilpayroll></login-lilpayroll>
				</div>
			</div>

			<!-- dashboard -->
			<dashboard-lilpayroll if={ page.name == "dashboard" }></dashboard-lilpayroll>

			<!-- cheat sheet -->
			<cheatsheet-lilpayroll if={ page.name == "cheat-sheet" }></cheatsheet-lilpayroll>

			<!-- payroll generator -->
			<payrollgenerator-lilpayroll if={ page.name == "payroll-generator" }></payrollgenerator-lilpayroll>

			<!-- payroll generator -->
			<viewpayroll-lilpayroll if={ page.name == "view-payroll" }></viewpayroll-lilpayroll>

			<!-- payroll generator -->
			<mypayslips-lilpayroll if={ page.name == "my-payslips" }></mypayslips-lilpayroll>
		</div>
	</div>


	<style type="text/css">
		.login-lilpayroll { min-height: 50vh; }
		.navbar-toggler { border: none; }
		nav .nav-link { 
			text-transform: uppercase; 
			font-weight: 700 !important;
		}
	</style>


	<script>

		pageAccessible(filter){
			if(filter != null){
				var arr = [];
				for (var i = 0; i < filter.length; i++) { arr.push(filter[i]["token"]); }

				try{
					var position = this.getMemory("usrSession")["_position"];
					if(arr.includes(position)){ return true; }
					else{ return false; }
				}
				catch(err){ return false; }
			}
			else{ return true; }

			return false;
		}

		main(){
			this.initTag("main");

			// this is used for routing!
				this.routeablePages = routeables;
				this.page = this.routeablePages[0];

			// route function - called after route change
				var thisRiot = this;
				route(function(pageName) {
					thisRiot.page = thisRiot.routeablePages.filter(function(thisPage) { 
						if(pageName != ""){
							if(thisPage.name == pageName){ 
								thisPage.active = true;
								return true; 
							}
							else{ 
								thisPage.active = false;
								return false;
							}
						}
						else{ return thisPage.active; }
					})[0] || {};


					// check for current logged user
					if(thisRiot.getMemory("usrSession") != null){ // already login
						if(thisRiot.page["name"] == "login"){ // prohibit going to login page
							thisRiot.changeLocation("dashboard");
						}

						// check for possible forced access here!
						if(thisRiot.page["positionFiltered"] != null){
							var usr = thisRiot.getMemory("usrSession");

							var assembleAllowedPersonel = [];
							for (var i = 0; i < thisRiot.page["positionFiltered"].length; i++) {
								assembleAllowedPersonel.push(thisRiot.page["positionFiltered"][i]["token"]);
							}

							if(!assembleAllowedPersonel.includes(usr["_position"])){
								thisRiot.changeLocation("dashboard");
							}
						}
						// ----------------------
					}
					else{ thisRiot.changeLocation("login"); } // always land to login if not logged

					thisRiot.update();
				});
		}
		this.main();
	</script>
</main-lilpayroll>