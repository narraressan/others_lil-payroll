<viewpayroll-lilpayroll>
	<div class="view-payroll">
		<div class="row">
			<div class="col-12">
				<div class="form-inline pull-right">
					<button class="btn btn-warning" onclick={ main }><span class="fa fa-refresh"></span></button>&nbsp;
					<a href="./pdf-printable/payrolls.html?billing={ filter }&user={ JSON.stringify(getMemory('usrSession')) }" target="_blank"  class="btn btn-primary"><span class="fa fa-download"></span>&nbsp;&nbsp;PDF</a>
					<select class="form-control action-dropd" onchange={ filterPayrolls }>
						<option>Select Payroll Group</option>
						<option each={ payrollFilters }>{ billing } - { coverage }</option>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<br>
				<table class="table table-striped table-hover table-bordered">
					<thead class="thead-inverse">
						<tr>
							<th>Employee</th>
							<th>Basic Salary</th>
							<th>Incentives</th>
							<th>Deductions</th>
							<th>Tax</th>
							<th>Net</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr each={ nth in companyEmployees.filter(showFilter) }>
							<td>
								<span class="surname" if={ nth["lastname"].trim() != "" }><b>{ nth["lastname"] }</b>, </span>
								{ nth["firstname"] }
								<br if={ nth["firstname"].trim() != "" }>
								<small if={ nth["email"].trim() != "" }>- { nth["email"] }</small>
							</td>
							<td>
								<!-- update this basic salary everytime you "blur" the input box -->
								<input type="text" class="form-control" value={ nth["gross_salary"] } onblur={ updateBasicSalary }>
								<a href="javascript:" onclick={ loadPrevSalary } if={ 0 < nth["prev_salary"] && nth["prev_salary"] != nth["gross_salary"] }><small>use previous salary: { nth["prev_salary"] }</small></a>
							</td>
							<td>
								<span class="factor-stated" onready={ getIncentiveFactors(nth["payslip_id"]) } id={ "incentive-" + nth["payslip_id"] }></span>
							</td>
							<td>
								<span class="factor-stated" onready={ getDeductionFactors(nth["payslip_id"]) } id={ "deduction-" + nth["payslip_id"] }></span>
							</td>
							<td>{ nth["tax"] }</td>
							<td>{ nth["net"] }</td>
							<td>
								<center>
									<div class="btn-group btn-group-sm">
										<button class="btn btn-primary" onclick={ stageToComputeNet } data-toggle="modal" data-target="#computeNet">Compute <b>NET S.</b></button>
										<!-- <button class="btn btn-success" onclick={ viewPayslip }>View Payslip</button> -->
									</div>
								</center>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="modal fade" id="computeNet">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<!-- you need to specify this every salary grading because there are instances that only 1 of the 2 sets of payment - gov't deductions are charged -->
						<div if={ computeNetForm == 1 }>
							<small>You need to specify at least <b>1</b> incentive AND deduction to proceed.</small>
							<hr>
							<form id="computeFactors">
								<table class="table table-hover table-bordered table-sm">
									<thead class="thead-inverse">
										<tr>
											<th class="text-center">Incentive</th>
											<th class="text-center">Amount</th>
										</tr>
									</thead>
									<tbody>
										<tr each={ incentivesList }>
											<th class="text-center factor-name">{ incentive }</th>
											<td class="amt"><input type="number" name={ token } class="form-control text-center input-sm" value={ findThisFactor(token) }></td>
										</tr>
									</tbody>
								</table>
								<br>
								<table class="table table-hover table-bordered table-sm">
									<thead class="thead-inverse">
										<tr>
											<th class="text-center">Deduction</th>
											<th class="text-center">Amount</th>
										</tr>
									</thead>
									<tbody>
										<tr each={ deductionsList }>
											<th class="text-center factor-name">{ deduction }</th>
											<td class="amt"><input type="number" name={ token } class="form-control text-center input-sm" value={ findThisFactor(token) }></td>
										</tr>
									</tbody>
								</table>
							</form>
						</div>

						<div if={ computeNetForm == 2 }>
							<a class="btn btn-primary btn-sm" data-toggle="collapse" href="#tax-factors">Edit Table Months of Tenure</a>
							<hr>
							<div class="collapse" id="tax-factors">
								<div class="form-group">
									<label>Taxable Months of Tenure</label>
									<input type="number" class="form-control tax_factor" name="taxable_Months" value="0">
								</div>
							</div>

							<div class="form-group">
								<label>Basic Income</label>
								<input type="number" class="form-control" readonly value={ stageEmployee["gross_salary"] }>
							</div>
							<div class="form-group">
								<label>Total Incentives</label>
								<input type="number" class="form-control" readonly value={ incentiveSummed }>
							</div>
							<div class="form-group">
								<label>Gross Income</label>
								<input type="number" class="form-control" name="grossIncome" readonly>
							</div>
							<div class="form-group">
								<label>Taxable - Income</label>
								<input type="number" class="form-control" name="taxableIncome" readonly>
							</div>
							<div class="form-group">
								<label>Monthly Tax</label>
								<input type="number" class="form-control" name="tax_amt" readonly value={ stageEmployee["net"] }>
							</div>
							<div class="form-group">
								<label>Total Deductions (Including Tax)</label>
								<input type="number" class="form-control" readonly value={ deductionSummed }>
							</div>
							<div class="form-group">
								<label>Net Income (Monthly)</label>
								<input type="number" class="form-control" name="monthlyNetIncome" readonly>
							</div>
							<div class="form-group">
								<label>Net Income (Semi-Monthly)</label>
								<input type="number" class="form-control" name="semiMonthlyNetIncome" readonly value={ stageEmployee["net"] }>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" onclick={ saveChanges }>Save changes</button>
						<div class="btn-group">
							<button type="button" class="btn btn-secondary" if={ computeNetForm > 1 } onclick={ prevForm }><b>Prev</b></button>
							<button type="button" class="btn btn-secondary" if={ computeNetForm < 2 && 0 < incentiveSummed && 0 < deductionSummed && 0 < stageEmployee["gross_salary"] } onclick={ nextForm }><b>Next</b></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<style type="text/css">
		.view-payroll { margin-top: 20px; }
		.action-dropd { margin-left: 20px; }
		.surname { text-transform: uppercase; }
		.factor-stated { display: block; }
		.amt { width: 50%; }
		.factor-name { padding-top: 10px; }
		.factor-name-tag { text-transform: uppercase; }
	</style>


	<script>
		this.companyEmployees = [];
		this.payrollFilters = [];

		this.incentivesList = [];
		this.deductionsList = [];

		// this.grossSalary = 0;
		// this.taxAmt = 0;

		this.computeNetForm = 1;
		self.incentiveSummed = 0;
		self.deductionSummed = 0;

		nextForm(){ 
			this.computeNetForm+=1;
			this.update();

			if(this.computeNetForm == 1){ this.loadExistingFactors(); }
			if(this.computeNetForm == 2){ this.computeTax(); }
		}

		prevForm(){ 
			this.computeNetForm-=1; 
			this.update();

			if(this.computeNetForm == 1){ this.loadExistingFactors(); }
			if(this.computeNetForm == 2){ this.computeTax(); }
		}

		computeTax(){
			try{ 
				// set default
				$("[name='taxable_Months']").val(12);

				var toCompute = {
					basic_salary: this.stageEmployee["gross_salary"],
					taxable_incentives: this.incentiveSummed,
					remaining_months: parseFloat($("[name='taxable_Months']").val()),
				}

				this.computeIncome(toCompute);
				this.update();
			}
			catch(err){
				// fail silently
			}

			var self = this;
			$(".tax_factor").on("input", function(e){
				var toCompute = {
					basic_salary: self.stageEmployee["gross_salary"],
					taxable_incentives: self.incentiveSummed,
					remaining_months: parseFloat($("[name='taxable_Months']").val()),
				}

				self.computeIncome(toCompute);
				self.update();
			});
		}

		computeIncome(toCompute){
			var self = this;
			$.ajax({
				method: "POST",
				url: host + "/compute-salary",
				data: toCompute,
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, computation failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						// self.toast("Filters retrieved");
						var temp = data["data"];

						$("[name='grossIncome']").val(temp["gross-income"]);
						$("[name='taxableIncome']").val(temp["taxable-income"]);
						$("[name='tax_amt']").val(temp["monthly-tax"]);

						self.deductionSummed = self.deductionSummed + parseFloat(temp["monthly-tax"]);

						var monthlyNetIncome = parseFloat(temp["gross-income"]) - self.deductionSummed;
						$("[name='monthlyNetIncome']").val(monthlyNetIncome);

						var semiMonthlyNetIncome = monthlyNetIncome / 2;
						$("[name='semiMonthlyNetIncome']").val(semiMonthlyNetIncome);

						self.update();
					}
					else{ self.toast("Sorry, computation failed"); }
				},
			})
		}

		loadPayroll_filters(company){
			// get payrolls on my company ONLY
			var self = this;
			$.ajax({
				method: "GET",
				url: host + "/get-payroll-filters?company=" + company,
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, securing available filters failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						// self.toast("Filters retrieved");
						self.payrollFilters = data["data"];
						self.update();
					}
					else{ self.toast("Sorry, securing available filters failed"); }
				},
			})
		}

		loadPayrolls(company){
			var self = this;
			$.ajax({
				method: "GET",
				url: host + "/get-payrolls?company=" + company,
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, securing available users failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						// self.toast("Employees retrieved");
						self.companyEmployees = data["data"];
						self.update();
					}
					else{ self.toast("Sorry, securing available users failed"); }
				},
			})
		}

		loadIncentives(){
			var self = this;
			$.ajax({
				method: "GET",
				url: host + "/get-incentive-factors?search=",
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, securing available incentives failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						// self.toast("Incentives retrieved");
						self.incentivesList = data["data"];
						self.update();
					}
					else{ self.toast("Sorry, securing available incentives failed"); }
				},
			})
		}

		loadDeductions(){
			// get payrolls on my company ONLY
			var self = this;
			$.ajax({
				method: "GET",
				url: host + "/get-deductions-factors?search=",
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, securing available deductions failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						// self.toast("Deductions retrieved");
						self.deductionsList = data["data"];
						self.update();
					}
					else{ self.toast("Sorry, securing available deductions failed"); }
				},
			})
		}

		this.existingFactors = [];
		loadExistingFactors(){
			var self = this;
			$.ajax({
				method: "GET",
				url: host + "/get-payslip-factors?payslip=" + this.stageEmployee["payslip_id"],
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, securing available factors failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						self.toast("Existing Factors retrieved");
						self.existingFactors = data["data"];

						self.incentiveSummed = 0;
						self.deductionSummed = 0;

						for (var i = 0; i < self.existingFactors.length; i++) {
							if(self.existingFactors[i]["type"] == 1){ 
								self.deductionSummed = parseFloat(self.existingFactors[i]["amt"]) + self.deductionSummed;
							}

							if(self.existingFactors[i]["type"] == 0){ 
								self.incentiveSummed = parseFloat(self.existingFactors[i]["amt"]) + self.incentiveSummed;
							}
						}

						self.update();
					}
					else{ self.toast("Sorry, securing available factors failed"); }
				},
			})
		}

		findThisFactor(factor){
			for (var i = 0; i < this.existingFactors.length; i++) {
				if(this.existingFactors[i]["factor_id"] == factor){ 
					return this.existingFactors[i]["amt"];
				}
			}
			return 0;
		}

		getIncentiveFactors(payslip_id){
			var self = this;
			$.ajax({
				method: "GET",
				url: host + "/get-payslip-factors?payslip=" + payslip_id,
				// callback functions
				success: function (data, status, xhr) {
					// find incentives - type = 0
					var incentives = "";

					for (var i = 0; i < data["data"].length; i++) {
						if(data["data"][i]["type"] == 0){
							var temp = "<span class='factor-name-tag'>" + data["data"][i]["factor"] + " - " + data["data"][i]["amt"] + "</span><br>";
							incentives += temp;
						}
					}

					$("#incentive-" + payslip_id).html(incentives);
				},
			})
		}

		getDeductionFactors(payslip_id){
			var self = this;
			$.ajax({
				method: "GET",
				url: host + "/get-payslip-factors?payslip=" + payslip_id,
				// callback functions
				success: function (data, status, xhr) {
					// find deductions - type = 1
					var deductions = "";

					for (var i = 0; i < data["data"].length; i++) {
						if(data["data"][i]["type"] == 1){
							var temp = "<span class='factor-name-tag'>" + data["data"][i]["factor"] + " - " + data["data"][i]["amt"] + "</span><br>";
							deductions += temp;
						}
					}

					$("#deduction-" + payslip_id).html(deductions);
				},
			})
		}

		computeColumns(targetClass){
			var total = 0;
			var target = $("." + targetClass);

			for (var i = 0; i < target.length; i++) {
				var amt = parseFloat($(target[i]).val());
				if(!isNaN(amt)){ total += amt; };
			}

			return total;
		}

		this.filter = "";
		filterPayrolls(){
			this.filter = $(event.target).val();
			this.update(); 
		}

		showFilter(item){
			var temp = item["billing"] + " - " + item["coverage"];
			if(this.filter == temp){ return item; }
		}

		this.stageEmployee = {};
		stageToComputeNet(item){ 
			this.computeNetForm = 1;
			this.stageEmployee = item["item"]["nth"];
			this.loadExistingFactors();
		}

		viewPayslip(item){ console.log(item["item"]["nth"]); }

		saveChanges(){
			var self = this;
			if(this.computeNetForm == 1){ this.upsertComputable(); }
			else if(this.computeNetForm == 2){ this.updateTax(); }
		}

		updateBasicSalary(item){
			var basicSalary = parseFloat($(event.target).val());
			var payslip = item["item"]["nth"]["payslip_id"];

			var self = this;
			$.ajax({
				method: "POST",
				url: host + "/update-basic-salary",
				data: {
					basic: basicSalary,
					payslip: payslip,
				},
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						self.toast("Basic Salary has been updated");
						$("#computeNet").modal("hide");
						self.main();
					}
					else{ self.toast("Sorry, update failed"); }
				},
			})
		}

		upsertComputable(){
			var self = this;
			$.ajax({
				method: "POST",
				url: host + "/upsert-computable-per-sched",
				data: {
					payslip: this.stageEmployee["payslip_id"],
					factors: $("#computeFactors").serializeArray()
				},
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						self.toast("Factors updated"); 
						self.main();
						$("#computeNet").modal("hide");
					}
					else{ self.toast("Sorry, update failed"); }
				},
			})
		}

		updateTax(){
			var tax = parseFloat($("[name='tax_amt']").val());
			var net = parseFloat($("[name='semiMonthlyNetIncome']").val());
			var payslip = this.stageEmployee["payslip_id"];

			var self = this;
			$.ajax({
				method: "POST",
				url: host + "/update-tax",
				data: {
					tax: tax,
					net: net,
					payslip: payslip,
				},
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						self.toast("Tax has been updated"); 
						self.main();
						$("#computeNet").modal("hide");
					}
					else{ self.toast("Sorry, update failed"); }
				},
			})
		}

		loadPrevSalary(item){
			var thisInput = $(event.target).parent().prev();
			thisInput.val(item["item"]["nth"]["prev_salary"]);
			thisInput.focus();
		}


		main(){
			this.initTag("view-payroll");

			try{
				// the payroll officer should always have a company!
				var company = this.getMemory("usrSession")["_company"];
				this.loadPayrolls(company);
				this.loadPayroll_filters(company);
				this.loadIncentives();
				this.loadDeductions();
			}
			catch(err){ console.log("error in filtering employees by company!"); }
		}
		this.main();
	</script>
</viewpayroll-lilpayroll>