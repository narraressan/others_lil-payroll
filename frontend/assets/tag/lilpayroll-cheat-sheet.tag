<cheatsheet-lilpayroll>
	<div class="row cheatsheet">
		<div class="col">
			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link active in" data-toggle="tab" href="#users">Users</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#company">Company</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#department">Department</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#position">Position</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#factors">Factors</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane fade show active" id="users">
					<p class="col-12 text-right">
						<button class="btn btn-warning" onclick={ getUsersList }><span class="fa fa-refresh"></span></button>
						<button class="btn btn-primary" data-toggle="modal" data-target="#addUser"><span class="fa fa-plus"></span> New User</button>
					</p>
					<table class="table table-striped table-hover table-bordered">
						<thead class="thead-inverse">
							<tr>
								<th>Email</th>
								<th>Password</th>
								<th>Employee ID</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Company</th>
								<th>Department</th>
								<th>Position</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<tr each={ users }>
								<td>
									<b class="editable">{ email }</b>
									<input type="text" class="form-control on-editable" name="email" value={ email } readonly>
								</td>
								<td>
									<b class="editable">{ password.slice(0, 10) + "..." }</b>
									<input type="text" class="form-control on-editable" name="password" value={ password } readonly>
								</td>
								<td>
									<b class="editable">{ employee_id }</b>
									<input type="text" class="form-control on-editable" name="employee_id" value={ employee_id }>
								</td>
								<td>
									<b class="editable">{ firstname }</b>
									<input type="text" class="form-control on-editable" name="firstname" value={ firstname }>
								</td>
								<td>
									<b class="editable">{ lastname }</b>
									<input type="text" class="form-control on-editable" name="lastname" value={ lastname }>
								</td>
								<td>
									<b class="editable">{ company }</b>
									<select class="form-control on-editable" name="company">
										<option></option>
										<option each={ nth in companies } value={ nth.token } selected={ nth.company == company }>{ nth.company }</option>
									</select>
								</td>
								<td>
									<b class="editable">{ department }</b>
									<select class="form-control on-editable" name="department">
										<option></option>
										<option each={ nth in departments } value={ nth.token } selected={ nth.department == department }>{ nth.department }</option>
									</select>
								</td>
								<td>
									<b class="editable">{ position }</b>
									<select class="form-control on-editable" name="position">
										<option></option>
										<option each={ nth in positions } value={ nth.token } selected={ nth.position == position }>{ nth.position }</option>
									</select>
								</td>
								<td hidden>
									<input type="text" name="credibles" class="form-control on-editable" readonly value={ _login }>
									<input type="text" name="token" class="form-control on-editable" readonly value={ _user }>
								</td>
								<td>
									<center>
										<div class="btn-group btn-group-sm">
											<button class="btn btn-success editable" id="editTrigger" onclick={ editMode }><span class="fa fa-edit"></span>&nbsp;&nbsp;EDIT</button>
											<button class="btn btn-danger editable" onclick={ deleteUser }><span class="fa fa-trash"></span>&nbsp;&nbsp;DEACTIVATE</button>
										</div>
										<div class="btn-group btn-group-sm">
											<button class="btn btn-warning on-editable" onclick={ editMode }><span class="fa fa-edit"></span>&nbsp;&nbsp;CANCEL</button>
											<button class="btn btn-primary on-editable" onclick={ updateUserDetails }><span class="fa fa-save"></span>&nbsp;&nbsp;SAVE</button>
										</div>
									</center>
								</td>
							</tr>
						</tbody>
					</table>

					<!-- add new user -->
					<div class="modal fade" id="addUser">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<form onsubmit={ addUserLogin }>
									<div class="modal-body">
										<div class="form-group">
											<label>Email</label>
											<input type="email" name="email" class="form-control" placeholder="johndoe@gmail.com">
										</div>

										<div class="form-group">
											<label>Password</label>
											<input type="password" name="password" class="form-control" placeholder="***********">
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane fade" id="company">
					<p class="col-12 text-right">
						<button class="btn btn-warning" onclick={ getCompanyList }><span class="fa fa-refresh"></span></button>
						<button class="btn btn-primary" onclick={ addCompany }><span class="fa fa-plus"></span> New Company</button>
					</p>
					<table class="table table-striped table-hover table-bordered">
						<thead class="thead-inverse">
							<tr>
								<th>Company</th>
								<th>ID</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<tr each={ companies }>
								<td>
									<b class="editable">{ company }</b>
									<input type="text" class="form-control on-editable" name="company" value={ company }>
								</td>
								<td>
									<span class="editable">{ token.slice(0, 10) + "..." }</span>
									<input type="text" class="form-control on-editable" name="token" value={ token } readonly>
								</td>
								<td>
									<center>
										<div class="btn-group btn-group-sm">
											<button class="btn btn-success editable" id="editTrigger" onclick={ editMode }><span class="fa fa-edit"></span>&nbsp;&nbsp;EDIT</button>
											<button class="btn btn-danger editable" onclick={ deleteCompany }><span class="fa fa-trash"></span>&nbsp;&nbsp;DELETE</button>
										</div>
										<div class="btn-group btn-group-sm">
											<button class="btn btn-warning on-editable" onclick={ editMode }><span class="fa fa-edit"></span>&nbsp;&nbsp;CANCEL</button>
											<button class="btn btn-primary on-editable" onclick={ updateCompany }><span class="fa fa-save"></span>&nbsp;&nbsp;SAVE</button>
										</div>
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="tab-pane fade" id="department">
					<p class="col-12 text-right">
						<button class="btn btn-warning" onclick={ getDepartmentList }><span class="fa fa-refresh"></span></button>
						<button class="btn btn-primary" onclick={ addDepartment }><span class="fa fa-plus"></span> New Department</button>
					</p>
					<table class="table table-striped table-hover table-bordered">
						<thead class="thead-inverse">
							<tr>
								<th>Department</th>
								<th>ID</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<tr each={ departments }>
								<td>
									<b class="editable">{ department }</b>
									<input type="text" class="form-control on-editable" name="department" value={ department }>
								</td>
								<td>
									<span class="editable">{ token.slice(0, 10) + "..." }</span>
									<input type="text" class="form-control on-editable" name="token" value={ token } readonly>
								</td>
								<td>
									<center>
										<div class="btn-group btn-group-sm">
											<button class="btn btn-success editable" id="editTrigger" onclick={ editMode }><span class="fa fa-edit"></span>&nbsp;&nbsp;EDIT</button>
											<button class="btn btn-danger editable" onclick={ deleteDepartment }><span class="fa fa-trash"></span>&nbsp;&nbsp;DELETE</button>
										</div>
										<div class="btn-group btn-group-sm">
											<button class="btn btn-warning on-editable" onclick={ editMode }><span class="fa fa-edit"></span>&nbsp;&nbsp;CANCEL</button>
											<button class="btn btn-primary on-editable" onclick={ updateDepartment }><span class="fa fa-save"></span>&nbsp;&nbsp;SAVE</button>
										</div>
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="tab-pane fade" id="position">
					<p class="col-12 text-right">
						<button class="btn btn-warning" onclick={ getPositionList }><span class="fa fa-refresh"></span></button>
						<button class="btn btn-primary" onclick={ addPosition }><span class="fa fa-plus"></span> New Position</button>
					</p>
					<table class="table table-striped table-hover table-bordered">
						<thead class="thead-inverse">
							<tr>
								<th>Position</th>
								<th>ID</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<tr each={ positions }>
								<td>
									<b class="editable">{ position }</b>
									<input type="text" class="form-control on-editable" name="position" value={ position }>
								</td>
								<td>
									<span class="editable">{ token.slice(0, 10) + "..." }</span>
									<input type="text" class="form-control on-editable" name="token" value={ token } readonly>
								</td>
								<td>
									<center>
										<div class="btn-group btn-group-sm">
											<button class="btn btn-success editable" id="editTrigger" onclick={ editMode }><span class="fa fa-edit"></span>&nbsp;&nbsp;EDIT</button>
											<button class="btn btn-danger editable" onclick={ deletePosition }><span class="fa fa-trash"></span>&nbsp;&nbsp;DELETE</button>
										</div>
										<div class="btn-group btn-group-sm">
											<button class="btn btn-warning on-editable" onclick={ editMode }><span class="fa fa-edit"></span>&nbsp;&nbsp;CANCEL</button>
											<button class="btn btn-primary on-editable" onclick={ updatePosition }><span class="fa fa-save"></span>&nbsp;&nbsp;SAVE</button>
										</div>
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="tab-pane fade" id="factors">
					<p class="col-12 text-right">
						<button class="btn btn-warning" onclick={ getFactorList }><span class="fa fa-refresh"></span></button>
						<button class="btn btn-primary" onclick={ addFactor }><span class="fa fa-plus"></span> New Factor</button>
					</p>
					<table class="table table-striped table-hover table-bordered">
						<thead class="thead-inverse">
							<tr>
								<th>Factor</th>
								<th>Type</th>
								<th>ID</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<tr each={ factors }>
								<td>
									<b class="editable">{ factor }</b>
									<input type="text" class="form-control on-editable" name="factor" value={ factor }>
								</td>
								<td>
									<b class="editable">{ status }</b>
									<select class="form-control on-editable" name="status">
										<option value="1" selected={ _status == "1" }>deduction</option>
										<option value="0" selected={ _status == "0" }>incentive</option>
									</select>
								</td>
								<td>
									<span class="editable">{ token.slice(0, 10) + "..." }</span>
									<input type="text" class="form-control on-editable" name="token" value={ token } readonly>
								</td>
								<td>
									<center>
										<div class="btn-group btn-group-sm">
											<button class="btn btn-success editable" id="editTrigger" onclick={ editMode }><span class="fa fa-edit"></span>&nbsp;&nbsp;EDIT</button>
											<button class="btn btn-danger editable" onclick={ deleteFactor }><span class="fa fa-trash"></span>&nbsp;&nbsp;DELETE</button>
										</div>
										<div class="btn-group btn-group-sm">
											<button class="btn btn-warning on-editable" onclick={ editMode }><span class="fa fa-edit"></span>&nbsp;&nbsp;CANCEL</button>
											<button class="btn btn-primary on-editable" onclick={ updateFactor }><span class="fa fa-save"></span>&nbsp;&nbsp;SAVE</button>
										</div>
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


	<style type="text/css">
		.cheatsheet { margin-top: 20px; }
		.nav-link { text-transform: uppercase; }
		.nav-link.active { font-weight: 700; }
		.tab-pane { padding-top: 20px; }
		.on-editable { display: none; }
		b.editable { text-transform: uppercase; }
		#users b.editable { text-transform: none; }
	</style>


	<script>
		// COMPANY FUNCTIONS
			this.companies = [];
			getCompanyList(){
				var self = this;
				$.ajax({
					method: "GET",
					url: host + "/get-companies?search=",
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, securing company list failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("Companies retrieved");
							self.companies = data["data"];
							self.update();
						}
						else{ self.toast("Sorry, securing company list failed"); }
					},
				})
			}

			updateCompany(event){
				var tr = $(event.target).closest("tr").find(".on-editable");
				var data = this.serializeForm2(tr);
				
				var self = this;
				$.ajax({
					method: "POST",
					url: host + "/upsert-company",
					data: data,
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("Company updated");
							self.getCompanyList();
						}
						else{ self.toast("Sorry, update failed"); }
					},
				})

				this.editMode();
			}

			addCompany(){ 
				this.companies.push({ company: "", token: "" });
				this.update();

				// open editable on added row
				try{
					var trArr = $("#company").find("tr");
					$($(trArr[trArr.length-1]).find("#editTrigger")[0]).click();
				}
				catch(err){ console.log("no companies found"); }
			}

			deleteCompany(){
				if(confirm("Are you sure to remove this COMPANY data?")){
					var tr = $(event.target).closest("tr").find(".on-editable");
					var data = this.serializeForm2(tr);

					// we do not delete directly the content in the db, instead we add a <deleted>...</deleted> tag in the name
					// NOT THE BEST IMPLIMENTATION
					data["company"] = "<deleted>" + data["company"] + "</deleted>";
					
					var self = this;
					$.ajax({
						method: "POST",
						url: host + "/upsert-company",
						data: data,
						// callback functions
						error: function (xhr, status, err) { self.toast("Sorry, removal failed"); },
						success: function (data, status, xhr) {
							if(data["data"] != null){ 
								self.toast("Company Deleted");
								self.getCompanyList();
							}
							else{ self.toast("Sorry, removal failed"); }
						},
					})

					this.editMode();
				}
			}

		// COMPANY FUNCTIONS
			this.departments = [];
			getDepartmentList(){
				var self = this;
				$.ajax({
					method: "GET",
					url: host + "/get-departments?search=",
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, securing department list failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("Departments retrieved");
							self.departments = data["data"];
							self.update();
						}
						else{ self.toast("Sorry, securing department list failed"); }
					},
				})
			}

			updateDepartment(event){
				var tr = $(event.target).closest("tr").find(".on-editable");
				var data = this.serializeForm2(tr);
				
				var self = this;
				$.ajax({
					method: "POST",
					url: host + "/upsert-department",
					data: data,
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("Department updated");
							self.getDepartmentList();
						}
						else{ self.toast("Sorry, update failed"); }
					},
				})

				this.editMode();
			}

			addDepartment(){ 
				this.departments.push({ department: "", token: "" });
				this.update();

				// open editable on added row
				try{
					var trArr = $("#department").find("tr");
					$($(trArr[trArr.length-1]).find("#editTrigger")[0]).click();
				}
				catch(err){ console.log("no departments found"); }
			}

			deleteDepartment(){
				if(confirm("Are you sure to remove this DEPARTMENT data?")){
					var tr = $(event.target).closest("tr").find(".on-editable");
					var data = this.serializeForm2(tr);

					// we do not delete directly the content in the db, instead we add a <deleted>...</deleted> tag in the name
					// NOT THE BEST IMPLIMENTATION
					data["department"] = "<deleted>" + data["department"] + "</deleted>";
					
					var self = this;
					$.ajax({
						method: "POST",
						url: host + "/upsert-department",
						data: data,
						// callback functions
						error: function (xhr, status, err) { self.toast("Sorry, removal failed"); },
						success: function (data, status, xhr) {
							if(data["data"] != null){ 
								self.toast("Department Deleted");
								self.getDepartmentList();
							}
							else{ self.toast("Sorry, removal failed"); }
						},
					})

					this.editMode();
				}
			}

		// COMPANY FUNCTIONS
			this.positions = [];
			getPositionList(){
				var self = this;
				$.ajax({
					method: "GET",
					url: host + "/get-positions?search=",
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, securing available positions failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("Positions retrieved");
							self.positions = data["data"];
							self.update();
						}
						else{ self.toast("Sorry, securing available positions failed"); }
					},
				})
			}

			updatePosition(event){
				var tr = $(event.target).closest("tr").find(".on-editable");
				var data = this.serializeForm2(tr);
				
				var self = this;
				$.ajax({
					method: "POST",
					url: host + "/upsert-position",
					data: data,
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("Position updated");
							self.getPositionList();
						}
						else{ self.toast("Sorry, update failed"); }
					},
				})

				this.editMode();
			}

			addPosition(){ 
				this.positions.push({ position: "", token: "" });
				this.update();

				// open editable on added row
				try{
					var trArr = $("#position").find("tr");
					$($(trArr[trArr.length-1]).find("#editTrigger")[0]).click();
				}
				catch(err){ console.log("no positions found"); }
			}

			deletePosition(event){
				if(confirm("Are you sure to remove this POSITION data?")){
					var tr = $(event.target).closest("tr").find(".on-editable");
					var data = this.serializeForm2(tr);

					// we do not delete directly the content in the db, instead we add a <deleted>...</deleted> tag in the name
					// NOT THE BEST IMPLIMENTATION
					data["position"] = "<deleted>" + data["position"] + "</deleted>";
					
					var self = this;
					$.ajax({
						method: "POST",
						url: host + "/upsert-position",
						data: data,
						// callback functions
						error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
						success: function (data, status, xhr) {
							if(data["data"] != null){ 
								self.toast("Position updated");
								self.getPositionList();
							}
							else{ self.toast("Sorry, update failed"); }
						},
					})

					this.editMode();
				}
			}

		// COMPANY FUNCTIONS
			this.factors = [];
			getFactorList(){
				var self = this;
				$.ajax({
					method: "GET",
					url: host + "/get-computable-factors?search=",
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, securing available factors failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("Factors retrieved");
							self.factors = data["data"];
							self.update();
						}
						else{ self.toast("Sorry, securing available factors failed"); }
					},
				})
			}

			updateFactor(event){
				var tr = $(event.target).closest("tr").find(".on-editable");
				var data = this.serializeForm2(tr);
				
				var self = this;
				$.ajax({
					method: "POST",
					url: host + "/upsert-computable-factor",
					data: data,
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("Factor updated");
							self.getFactorList();
						}
						else{ self.toast("Sorry, update failed"); }
					},
				})

				this.editMode();
			}

			addFactor(){ 
				this.factors.push({ factor: "", status: "1", token: "" });
				this.update();

				// open editable on added row
				try{
					var trArr = $("#factors").find("tr");
					$($(trArr[trArr.length-1]).find("#editTrigger")[0]).click();
				}
				catch(err){ console.log("no factors found"); }
			}

			deleteFactor(event){
				if(confirm("Are you sure to remove this 'FACTOR' data?")){
					var tr = $(event.target).closest("tr").find(".on-editable");
					var data = this.serializeForm2(tr);

					// we do not delete directly the content in the db, instead we add a <deleted>...</deleted> tag in the name
					// NOT THE BEST IMPLIMENTATION
					data["factor"] = "<deleted>" + data["factor"] + "</deleted>";
					
					var self = this;
					$.ajax({
						method: "POST",
						url: host + "/upsert-computable-factor",
						data: data,
						// callback functions
						error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
						success: function (data, status, xhr) {
							if(data["data"] != null){ 
								self.toast("Factor updated");
								self.getFactorList();
							}
							else{ self.toast("Sorry, update failed"); }
						},
					})

					this.editMode();
				}
			}

		// USER FUNCTIONS
			this.users = [];
			getUsersList(){
				var self = this;
				$.ajax({
					method: "GET",
					url: host + "/get-users",
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, securing available users failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("Positions retrieved");
							self.users = data["data"];
							self.update();
						}
						else{ self.toast("Sorry, securing available users failed"); }
					},
				})
			}

			updateUserDetails(event){
				var tr = $(event.target).closest("tr").find(".on-editable");
				var data = this.serializeForm2(tr);

				var self = this;
				$.ajax({
					method: "POST",
					url: host + "/upsert-user-details",
					data: data,
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("User updated");
							self.getUsersList();
						}
						else{ self.toast("Sorry, update failed"); }
					},
				})

				this.editMode();
			}

			addUserLogin(event){
				var data = this.serializeForm(event);
				data["token"] = "";
				
				var self = this;
				$.ajax({
					method: "POST",
					url: host + "/upsert-user-credible",
					data: data,
					// callback functions
					error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
					success: function (data, status, xhr) {
						if(data["data"] != null){ 
							self.toast("User updated");
							$("#addUser").modal("hide");
							self.getUsersList();
						}
						else{ self.toast("Sorry, update failed"); }
					},
				})

				event.preventDefault();
				event.target.reset();
			}

			deleteUser(event){
				if(confirm("Are you sure to deactivate this USER?")){
					var tr = $(event.target).closest("tr").find(".on-editable");
					var data = this.serializeForm2(tr);

					var newData = { active: 0, token: data["credibles"] };
					
					var self = this;
					$.ajax({
						method: "POST",
						url: host + "/deactivate-users",
						data: newData,
						// callback functions
						error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
						success: function (data, status, xhr) {
							if(data["data"] != null){ 
								self.toast("User updated");
								self.getUsersList();
							}
							else{ self.toast("Sorry, update failed"); }
						},
					})

					this.editMode();
				}
			}




		editMode(event){
			try{
				var tr = $(event.target).closest("tr");
				if($(tr).find(".on-editable").is(":visible")){
					$(tr).find(".on-editable").hide()
					$(tr).find(".editable").show();
				}
				else{
					$(tr).find(".on-editable").show()
					$(tr).find(".editable").hide();
				}
			}
			catch(err){ console.log("event - target not found!"); }
		}

		main(){
			this.initTag("cheat-sheet");

			this.getCompanyList();
			this.getDepartmentList();
			this.getPositionList();
			this.getUsersList();
			this.getFactorList();
		}
		this.main();
	</script>
</cheatsheet-lilpayroll>