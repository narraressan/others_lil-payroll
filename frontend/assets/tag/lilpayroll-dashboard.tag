<dashboard-lilpayroll>
	<div class="row dashboard">
		<div class="col-lg-4 col-md-4">
			<div class="card">
				<h4 class="card-header"><small>ID#:</small> { getUserSession("employee_id") }</h4>
				<div class="card-block">
					<h5 class="card-title">{ getUserSession("firstname") + " " + getUserSession("lastname") }</h5>
					<h6 class="card-subtitle text-muted">{ getUserSession("position") }</h6>
				</div>
				<img style="width: 100%; display: block;" src="https://placeimg.com/640/480/people" class="img-responsive">
				<div class="card-block">
					<p class="card-text lead">
						<small>Email:</small> { getUserSession("email") }<br>
						<small>Company:</small> { getUserSession("company") }<br>
						<small>Department:</small> { getUserSession("department") }<br>
					</p>
				</div>
				<div class="card-footer text-muted text-xs-center">
					Edit: 
					<button class="btn btn-primary" onclick={ enableEditable_login }>Login Credential</button>
					<button class="btn btn-primary" onclick={ enableEditable_details }>Profile</button>
				</div>
			</div>
		</div>

		<div class="col">
			<div class="card">
				<div class="card-block">
				
					<legend>My Profile</legend><hr>

					<form onsubmit={ updateUserCredible }>
						<div class="row">
							<div class="form-group col-6">
								<label>Email</label>
								<input type="email" class="form-control" name="email" readonly={ !enableEdit_login } value={ getUserSession("email") }>
							</div>
							<div class="form-group col-6">
								<label>Password</label>
								<input type="password" class="form-control" name="password" readonly={ !enableEdit_login } value={ getUserSession("password") } required>
							</div>

							<p class="text-right col" if={ enableEdit_login }>
								<i class="pull-left lead"><small>You will be logged out momentarily upon update of your account</small></i>
								<button class="btn btn-primary" disabled={ !enableEdit_login } type={ "button": !enableEdit_login, "submit": enableEdit_login }><span class="fa fa-save"></span> Save</button>
								<button class="btn btn-default" disabled={ !enableEdit_login } type="button" onclick={ disableEditable_login }><span class="fa fa-save"></span> Cancel</button>
							</p>
						</div>
						<hr>
					</form>

					<form onsubmit={ updateUserDetails }>
						<div class="row">
							<div class="form-group col-6">
								<label>First Name</label>
								<input type="text" class="form-control" name="firstname" readonly={ !enableEdit_details } value={ getUserSession("firstname") } required>
							</div>
							<div class="form-group col-6">
								<label>Last Name</label>
								<input type="text" class="form-control" name="lastname" readonly={ !enableEdit_details } value={ getUserSession("lastname") } required>
							</div>

							<p class="text-right col" if={ enableEdit_details }>
								<i class="pull-left lead"><small>You will be logged out momentarily upon update of your account</small></i>
								<button class="btn btn-primary" disabled={ !enableEdit_details } type={ "button": !enableEdit_details, "submit": enableEdit_details }><span class="fa fa-save"></span> Save</button>
								<button class="btn btn-default" disabled={ !enableEdit_details } type="button" onclick={ disableEditable_details }><span class="fa fa-save"></span> Cancel</button>
							</p>
						</div>
						<hr>
						
						<div class="row">
							<div class="form-group col-6">
								<label>Company</label>
								<input type="text" class="form-control" readonly value={ getUserSession("company") }>
								<input type="text" class="form-control" hidden name="company" readonly value={ getUserSession("_company") }>
							</div>
							<div class="form-group col-6">
								<label>Department</label>
								<input type="text" class="form-control" readonly value={ getUserSession("department") }>
								<input type="text" class="form-control" hidden name="department" readonly value={ getUserSession("_department") }>
							</div>
							<div class="form-group col-6">
								<label>Position</label>
								<input type="text" class="form-control" readonly value={ getUserSession("position") }>
								<input type="text" class="form-control" hidden name="position" readonly value={ getUserSession("_position") }>
							</div>
							<div class="form-group col-6">
								<label>Employee ID</label>
								<input type="text" class="form-control" name="employee_id" readonly value={ getUserSession("employee_id") }>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>


	<style type="text/css">
		.dashboard { margin-top: 20px; }
	</style>


	<script>
		this.enableEdit_login = false;
		this.enableEdit_details = false;

		enableEditable_login(){ 
			this.enableEdit_login = true;
			$("[name='password']").val("");
		}
		disableEditable_login(){ 
			this.enableEdit_login = false;
			$("[name='password']").val("********************");
			this.update();
		}

		enableEditable_details(){ this.enableEdit_details = true; }
		disableEditable_details(){ 
			this.enableEdit_details = false; 
			this.update();
		}

		getUserSession(param){
			try{ 
				var temp = this.getMemory("usrSession")[param];
				if(temp == null || temp === undefined){ return ""; }
				else{ return temp; }
			}
			catch(err){ return ""; }
		}

		updateUserCredible(event){
			var form = event;

			var user = this.getMemory("usrSession");
			var pass = this.getMemory("mypass");
			var data = this.serializeForm(form);
				
			var userCredible = {
				email: data["email"],
				password: data["password"],
				token: user["_login"],
			}

			var self = this;
			$.ajax({
				method: "POST",
				url: host + "/upsert-user-credible",
				data: userCredible,
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						self.toast("Successful update");
						self.enableEdit_login = false;
						self.update();
						self.logoutSession();
					}
					else{ self.toast("Sorry, update failed"); }
				},
			})

			form.preventDefault();
		}

		updateUserDetails(event){
			var form = event;

			var user = this.getMemory("usrSession");
			var data = this.serializeForm(form);
				
			var userDetails = {
				firstname: data["firstname"],
				lastname: data["lastname"],
				employee_id: data["employee_id"],
				company: data["company"],
				department: data["department"],
				position: data["position"],
				credibles: user["_login"],
				token: user["_user"],
			}
			
			var self = this;
			$.ajax({
				method: "POST",
				url: host + "/upsert-user-details",
				data: userDetails,
				// callback functions
				error: function (xhr, status, err) { self.toast("Sorry, update failed"); },
				success: function (data, status, xhr) {
					if(data["data"] != null){ 
						self.toast("Successful update");
						self.enableEdit_details = false;
						self.update();
						self.logoutSession();
					}
					else{ self.toast("Sorry, update failed"); }
				},
			})

			form.preventDefault();
		}


		main(){
			this.initTag("dashboard");
		}
		this.main();

	</script>
</dashboard-lilpayroll>