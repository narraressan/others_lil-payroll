
var mixinFunction = {
	initTag: function(tag){ console.log(tag + ": tag initialized!"); },
	serializeForm: function(event){
		var temp = $(event.target).serializeArray(); // event.target = form
		return this.computeSerial(temp);
	},
	serializeForm2: function(tr){
		var temp = $(tr).serializeArray();
		return this.computeSerial(temp);
	},
	computeSerial: function(temp){
		var formData = {};

		// transform serialize to my JSON format
		for (var i = 0; i < temp.length; i++) { 
			try{ formData[temp[i]["name"]] = temp[i]["value"]; }
			catch(err){ console.log("err"); } 
		}

		return formData;
	},
	toast: function(text){
		toastr.options = { positionClass: "toast-bottom-right", timeOut: "5000" };
		toastr.info(text);
	},
	inMemory: function(name, data){ localStorage.setItem(name, JSON.stringify(data)); },
	getMemory: function(name){ return JSON.parse(localStorage.getItem(name)); },
	forgetMemory: function(name){ return localStorage.removeItem(name); },
	changeLocation: function(newLocation){ window.location.replace("#" + newLocation); },
	reloadPage: function(){ window.location.reload(); },
	logoutSession: function(){ 
		this.forgetMemory("usrSession");
		this.reloadPage();
	},
};