
// backend host url
// var host = "http://localhost:81/others_lil-payroll/backend/controller.php";
var host = "http://localhost/backend/controller.php";

// pages-available
var routeables = [
	{ name: "login", active: true, positionFiltered: null },
	{ name: "dashboard", active: false, positionFiltered: null }, // view & update profile details
	{ name: "my-payslips", active: false, positionFiltered: null }, // view "MY" payslips - history
	{ 
		name: "view-payroll", // view & download all payrolls and payroll history as PDF - view data for same company only
		active: false, 
		// list of allowed positions (defined by tokens) to access this page
			// this is not ideal to configure directly in frontend 
			// - but because of time constraints, we have no choice :(
			positionFiltered: [
				{ alias: "payroll-officer", token: "28lnsmzQCQ_wgT2wDlKqoBwlOsmSzhK3WVCzyGssIrdi5xodWP" },
				] 
	},
	{ 
		name: "payroll-generator", // generate and/or update payslips for all employees - on same company
		active: false, 
		positionFiltered: [{ alias: "payroll-officer", token: "28lnsmzQCQ_wgT2wDlKqoBwlOsmSzhK3WVCzyGssIrdi5xodWP" },] 
	},
	{ 
		name: "cheat-sheet", // generate and/or update payslips for all employees - on same company
		active: false, 
		positionFiltered: [{ alias: "admin", token: "1" },] 
	},
];